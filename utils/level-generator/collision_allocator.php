<?php

/**
 * @param $coord
 * @param $typeId
 * @return array
 * @throws Exception
 */
const TREE_1 = 0;
const TREE_2 = 1;
const BREAD = 2;
const CORN = 3;
const HAY = 4;
const MUD = 5;
const LOG = 6;
const ROCK = 7;
const HOLE = 8;
const FOX_RUNNING = 9;
const FOX_STAND = 10;

$getCollisionPoints = function ($typeId) {

    $coords = [];

    switch ($typeId) {

        case TREE_1:
            $coords['x'] = 156;
            $coords['y'] = 399;
            break;

        case TREE_2:
            $coords['x'] = 304;
            $coords['y'] = 397;
            break;

        case BREAD:
        case CORN:
            $coords['x'] = 34;
            $coords['y'] = 111;
            break;

        case HAY:
            $coords['x'] = 62;
            $coords['y'] = 158;
            break;

        case MUD:
            $coords['x'] = 130.5;
            $coords['y'] = 64;
            break;

        case LOG:
            $coords['x'] = 58;
            $coords['y'] = 110;
            break;

        case ROCK:
            $coords['x'] = 2;
            $coords['y'] = 36;
            break;

        case HOLE:
            $coords['x'] = 130.5;
            $coords['y'] = 59;
            break;

        case FOX_RUNNING:
            $coords['x'] = 180;
            $coords['y'] = 210;
            break;

        case FOX_STAND:
            $coords['x'] = 180;
            $coords['y'] = 180;
            break;

        default:
            throw new Exception('Type not found!');
    }

    return $coords;
};
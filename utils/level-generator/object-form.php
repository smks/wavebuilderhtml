<div class="game-object" data-id="0">
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse0">Game Object 0</a>
                </h4>
            </div>

            <div id="collapse0" class="panel-collapse collapse in">
                <div class="panel-body">
                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="type">Type</label>

                        <div class="col-md-4">
                            <select name="objects[0][type]" class="form-control go-type">
                                <?php foreach ($typesList as $key => $type) : ?>
                                    <option data-type="<?= $type ?>" value="<?= $key ?>"><?= $type ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="xPos">X Position</label>

                        <div class="col-md-4">
                            <input name="objects[0][xPos]" type="text" placeholder="1024"
                                   class="form-control input-md go-xPos"
                                   required="">
                            <span class="help-block">Where it should be positioned X</span>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="yPos">Y Position</label>

                        <div class="col-md-4">
                            <input name="objects[0][yPos]" type="text" placeholder="420 - 768"
                                   class="form-control input-md go-yPos"
                                   required="">
                            <span class="help-block">Where to place the enemy</span>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="delayBefore">Delay Before</label>

                        <div class="col-md-4">
                            <input name="objects[0][delayBefore]" type="text" placeholder="1000"
                                   class="form-control input-md go-delayBefore" required="">
                            <span class="help-block">How long before it shows? (milliseconds)</span>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="delayAfter">Delay After</label>

                        <div class="col-md-4">
                            <input name="objects[0][delayAfter]" type="text" placeholder="1200"
                                   class="form-control input-md go-delayAfter" required="">
                            <span class="help-block">How long to wait until next object?</span>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="speed">Speed</label>

                        <div class="col-md-4">
                            <input name="objects[0][speed]" type="text" placeholder="1"
                                   class="form-control input-md go-speed" required="">
                            <span class="help-block">How fast should it move (1) default normal speed</span>
                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="grounded">Grounded</label>

                        <div class="col-md-4">
                            <input name="objects[0][grounded]" type="checkbox" class="checkbox go-grounded">
                            <span class="help-block">Should the player always appear above the object</span>
                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="speed">Snippets</label>

                        <div class="col-md-4">
                            <select name="objects[0][snippet]" class="form-control go-snippet" disabled="disabled">
                                <option data-type="none" value="0">-- Select Object Snippet --</option>
                                <?php foreach ($snippetsList as $key => $type) : ?>
                                    <option data-type="<?= $type ?>" value="<?= $key ?>"><?= $type ?></option>
                                <?php endforeach; ?>
                            </select>
                            <span class="help-block">A list of prebuilt object snippets for reuse</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
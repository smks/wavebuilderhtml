var $ = $ || {};

$(document).ready(function () {

    const MIN_Y = 240;
    const MAX_Y = 1080;
    const AUTOMATE_SPEED = 25;

    var isAutomating = false;
    var addObjectBtn = $('#add-object-btn');
    var randomObjectBtn = $('#random-object-btn');
    var removeObjectBtn = $('#remove-object-btn');
    var automate = $('#automate');
    var typeLength = $('.game-object').last().find('.go-type').find('option').length;

    automate.on('click', function () {

        var self = $(this);

        if (isAutomating == true) {
            self.text('Start Automation Level Generator');
            self.removeClass('btn-danger');
            $('body').css('background-color', '#ffffff');
            isAutomating = false;
            return;
        }

        isAutomating = true;

        self.text('Stop Automation Level Generator');
        self.addClass('btn-danger');

        $('body').css('background-color', '#EBFAE8');

        // fill first latest game object with random values
        randomObjectBtn.click();

        // automate Loop
        automateBuildLoop();
    });

    function automateBuildLoop() {

        var def1 = $.Deferred();
        var def2 = $.Deferred();

        addObjectBtn.trigger('click', def1);
        randomObjectBtn.trigger('click', def2);

        if (isAutomating) {
            setTimeout(automateBuildLoop, AUTOMATE_SPEED);
        }
    }

    /**
     * Add a new object
     */
    addObjectBtn.on('click', function (e) {

        var latestGameObject = getLatestGameObj();
        var latestGameObjectContent = getHtmlContent(latestGameObject);
        var latestGameObjectDataId = getGameObjId(latestGameObject);

        var __ret = updateGameObjContent(latestGameObjectDataId, latestGameObjectContent);
        var newId = __ret.newId;
        var newGameObject = __ret.newGameObject;

        var toggleGameObj = latestGameObject.find('a');
        if (toggleGameObj.hasClass('collapsed') == false) {
            toggleGameObj.click();
        }

        latestGameObject.after(
            '<div class="game-object" data-id="' + newId + '">' +
            newGameObject +
            '</div>'
        );

    });

    /**
     * Add random values to latest game object
     */
    randomObjectBtn.on('click', function (e) {
        randomiseGameObj(getLatestGameObj());
    });

    removeObjectBtn.on('click', function (e) {
        removeLatest(getLatestGameObj());
    });

    /** FUNCTIONS **/

    /**
     *
     * @returns {*|jQuery}
     */
    function getLatestGameObj() {
        return $('.game-object').last();
    }

    /**
     *
     * @param gameObj
     * @returns {*}
     */
    function getHtmlContent(gameObj) {
        return gameObj.html();
    }

    /**
     *
     * @param gameObj
     * @returns {Number}
     */
    function getGameObjId(gameObj) {
        return parseInt(gameObj.data('id'), 10);
    }

    /**
     *
     * @param latestGameObjectDataId
     * @param latestGameObjectContent
     * @returns {{newId: *, newGameObject: string}}
     */
    function updateGameObjContent(latestGameObjectDataId, latestGameObjectContent) {
        var idString = '\\[' + latestGameObjectDataId + '\\]';
        var idAccStr = 'collapse' + latestGameObjectDataId;
        var idRe = new RegExp(idString, 'g');
        var idAcc = new RegExp(idAccStr, 'g');
        var newId = latestGameObjectDataId + 1;
        var newGameObjectHtml = latestGameObjectContent
            .replace(idRe, '[' + newId + ']')
            .replace(idAcc, 'collapse' + newId);

        var newGameObject = newGameObjectHtml.replace('Game Object ' + latestGameObjectDataId, 'Game Object ' + newId);
        return {newId: newId, newGameObject: newGameObject};
    }

    /**
     *
     * @param latestGameObj
     */
    var randomiseGameObj = function (latestGameObj) {

        var type = latestGameObj.find('.go-type');
        var speed = latestGameObj.find('.go-speed')
        var xPos = latestGameObj.find('.go-xPos');
        var yPos = latestGameObj.find('.go-yPos');
        var delayBefore = latestGameObj.find('.go-delayBefore');
        var delayAfter = latestGameObj.find('.go-delayAfter')
        var grounded = latestGameObj.find('.go-grounded');

        type.val(getRandomInt(0, typeLength - 1));
        xPos.val(1920);
        yPos.val(getRandomInt(MIN_Y, MAX_Y));
        delayBefore.val(0);
        delayAfter.val(getRandomInt(0, 2000));

        console.log("type len: " + typeLength);
        console.log(type.val());

        if (type.val() == 9) {
            speed.val(1.5);
        } else {
            speed.val(1);
        }

        grounded.prop('checked', false);

        // if trees, show at the top
        if (type.val() <= 1) {
            grounded.prop('checked', true);
            yPos.val(0);
        }

        // check to grounded true for mud and holes
        if (type.val() == 5 || type.val() == 8) {
            grounded.prop('checked', true);
        }
    };

    /**
     * Returns a random integer between min (inclusive) and max (inclusive)
     * Using Math.round() will give you a non-uniform distribution!
     */
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    /**
     *
     * @param latestGameObj
     */
    function removeLatest(latestGameObj) {

        if ($('.game-object').length == 1) {
            alert('Cannot remove only game object');
            return;
        }

        latestGameObj.remove();
    }

});
<?php
$typesList = require_once('get_types.php');
?>
<!DOCTYPE html>
<html>
<head>
    <title>Level Builder</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<!-- Second navbar for categories -->
<nav class="navbar navbar-default navbar-inverse">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Ronnie The Rooster</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <!-- <li><a href="#">Home</a></li> -->
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
<!-- /.navbar -->

<div class="container">
    <div class="row" style="text-align: center;">
        <img class="image-responsive" src="image.png" alt=""/>
    </div>
    <div class="row" style="text-align: center;">
        <h1>Create a Level</h1>
    </div>
    <br>

    <div class="row">
        <div class="col-md-offset-4 col-md-4">
            <button id="automate" type="button" class="btn btn-block btn-default">Automate Level Generation</button>
        </div>
    </div>
    <br/>

    <div class="row">
        <div class="col-md-offset-4 col-md-4">
            <button id="add-object-btn" type="button" class="btn btn-block btn-primary">Add object</button>
        </div>
    </div>
    <br/>

    <div class="row">
        <div class="col-md-offset-4 col-md-4">
            <button id="random-object-btn" type="button" class="btn btn-block btn-info">Random Latest</button>
        </div>
    </div>
    <br/>

    <div class="row">
        <div class="col-md-offset-4 col-md-4">
            <button id="remove-object-btn" type="button" class="btn btn-block btn-danger">Remove Latest</button>
        </div>
    </div>
    <hr/>
    </br>
    <div class="row">
        <form class="form-horizontal" action="generator.php" method="post">
            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="level">Level ID</label>

                <div class="col-md-4">
                    <input id="level" name="level" type="number" placeholder="1-100" class="form-control input-md"
                           required="">
                    <span class="help-block">What Level is this?</span>
                </div>
            </div>
            <hr>
            <fieldset>
                <?php include('object-form.php'); ?>
                <hr/>
                <div id="submit-level-btn" class="form-group">
                    <label class="col-md-4 control-label" for="speed">All done?</label>

                    <div class="col-md-4">
                        <button type="submit" class="btn btn-default">Publish</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
</body>
<script src="scripts/jquery.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="scripts/level.js"></script>
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<style type="text/css">
    html * {
        font-family: 'Open Sans', sans-serif !important;
        color: #4r4r4r;
    }

    .help-block {
        color: #B5B5B5;
    }
</style>
</html>
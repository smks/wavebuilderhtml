<?php
$typesJson = file_get_contents('../../assets/data/types/types.json');
$typesList = json_decode($typesJson, true)['types'];
return $typesList;
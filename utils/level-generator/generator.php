<?php
/**
 * Created by PhpStorm.
 * User: Shaun Stone
 * Date: 30/05/2015
 * Time: 09:18
 */

 ini_set('max_input_vars', 1000000);

 require 'collision_allocator.php';

if (empty($_POST) == false) {
    $formData = $_POST;
    $levelId = filter_var($formData['level'], FILTER_VALIDATE_INT);
    $gameObjectEntities = $formData['objects'];
    $types = require_once 'get_types.php';

    foreach ($gameObjectEntities as $key => &$obj) {
        $obj['typeId'] = $obj['type'];
        $obj['type'] = $types[$obj['type']];

        $collisionPoints = $getCollisionPoints($obj['typeId']);
        $obj['collisionPointX'] = $collisionPoints['x'];
        $obj['collisionPointY'] = $collisionPoints['y'];

        if (array_key_exists('grounded', $obj)) {
            unset($obj['grounded']);
            $obj['grounded'] = true;
        } else {
            $obj['grounded'] = false;
        }
    }

    $gameObjectEntities = json_encode($gameObjectEntities);

    $file = __DIR__ . '/../../assets/data/levels/' . $levelId . '.json';
    $fh = fopen($file, 'w');
    fwrite($fh, $gameObjectEntities);
    fclose($fh);

    if (file_exists($file)) {
        $result = "<h2 class=\"text-success\">Successfully created level {$levelId} at: \"" . realpath($file) . '"</h2>';
        $statusCode = 201;
    } elseif (isset($_POST)) {
        $result = "<h2 class=\"text-success\">Failed to create level {$levelId} at: \"" . realpath($file) . '"</h2>';
        $statusCode = 400;
    }
} else {
    $result = 'Nothing processed';
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Result</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container" style="margin-top: 50px">

    <?php if ($result) : ?>
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <?= $result ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-offset-1 col-md-12">
                <a href="index.php" class="btn btn-default btn-lg">
                    Create Level
                </a>
            </div>
        </div>
    <?php endif; ?>
</div>

</body>
</html>

<style>
    html * {
        font-family: 'Open Sans', sans-serif !important;
        color: #777777;
    }

    body {
        background-color: rgb(250, 250, 250) !important;
    }
</style>
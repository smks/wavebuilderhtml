<?php
$dirPath = __DIR__ . '/../../assets/data/game-object-snippets';
$dirIter = new DirectoryIterator($dirPath);
$snippetsList = [];
foreach ($dirIter as $file) {
    if (!$file->isDir() && $file->getExtension() == 'json') {
        $snippetsList[$file->getFilename()] = $file->getBasename('.json');
    }
}
return $snippetsList;
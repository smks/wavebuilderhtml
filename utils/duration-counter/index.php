<?php

$dir = __DIR__ . '/../../assets/data/waves';
$files = scandir($dir);

/**
 * @param $file
 * @return array|bool
 */
$totals = function($file) use ($dir) {

    $duration = 0;

    $content = json_decode(file_get_contents($dir . '/' . $file));

    if (empty($content)) {
        return false;
    }

    $cornCount = 0;

    foreach ($content as $go) {
        $duration += (int)$go->delayAfter;

        if ($go->typeId == 3) {
            $cornCount++;
        }

    }

    $totalObjects = count($content);

    $duration += 3000;

    return array($totalObjects, $duration, $cornCount);
};

sort($files, SORT_NUMERIC);

function formatMilliseconds($milliseconds, $timeFormat = false) {
    $seconds = floor($milliseconds / 1000);
    $minutes = floor($seconds / 60);
    $milliseconds = $milliseconds % 1000;
    $seconds = $seconds % 60;
    $minutes = $minutes % 60;

    if ($timeFormat) {
        $format = '%02u:%02u:%02u';
    } else {
        $format = '%02u minutes, %02u seconds and %02u milliseconds';
    }
    $time = sprintf($format, $minutes, $seconds, $milliseconds);
    return $time;
}

echo '<h1>Ronnie The Rooster Waves</h1><hr>';

foreach ($files as $key => $f) {

    if ($f === '999.json') {
        continue;
    }

    if (!strstr($f, '.json')) {
        continue;
    }

    $id = basename($f, '.json');

    list($totalObjects, $duration, $cornCount) = $totals($f);

    if ($duration === false) {
        echo "<p>Wave: <b>" . $id . "</b> empty --------- </p>" ;
        continue;
    }

    if ($duration > 20000 && $duration < 120000) {
        $styles = 'style="color: green;"';
    } else {
        $styles = 'style="color: red;"';
    }

    echo sprintf('<p %s> Wave: <b>%s</b> lasts for: <b>%s</b> ---- Total Objects <b>%s</b> ---- Corn Count: <b>%s</b></p>' . PHP_EOL,
        $styles,
        $id,
        formatMilliseconds($duration),
        $totalObjects,
        $cornCount
    );
} ?>

<br>
<br>
<br>
<br>

<?php

foreach ($files as $key => $f) {

    if ($f === '999.json') {
        continue;
    }

    if (!strstr($f, '.json')) {
        continue;
    }

    list($totalObjects, $duration, $cornCount) = $totals($f);

    echo formatMilliseconds($duration, true) . '<br>';
} ?>

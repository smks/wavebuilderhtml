<?php
/**
 * Created by PhpStorm.
 * User: Shaun Stone
 * Date: 01/08/2015
 * Time: 09:18
 */

ini_set('max_input_vars', 1000000);

$snippetsPath = __DIR__ . '/../../assets/data/game-object-snippets/';

$result = '';

if (empty($_POST) == false) {

    $formData = $_POST;

    $levelId = filter_var($formData['level'], FILTER_VALIDATE_INT);

    $file = __DIR__ . '/../../assets/data/levels/' . $levelId . '.json';

//    if (file_exists($file)) {
//        $result = "<h2 class=\"text-danger\">Json already exists for level {$levelId}</h2>";
//    }

    if ($result == '') {
        $gameObjectEntities = $formData['objects'];

        $fullSnippetList = [];

        foreach ($gameObjectEntities as $key => $obj) {

            $jsonFile = $obj['type'];

            $jsonSnippet = json_decode(file_get_contents($snippetsPath . $jsonFile));

            if (json_last_error() != JSON_ERROR_NONE) {
                throw new Exception("JSON not valid!");
            }

            foreach ($jsonSnippet as $val) {
                array_push($fullSnippetList, $val);
            }
        }

        $gameObjectEntities = json_encode($fullSnippetList);

        $fh = fopen($file, 'w');
        fwrite($fh, $gameObjectEntities);
        fclose($fh);

        if (file_exists($file)) {
            $result = "<h2 class=\"text-success\">Successfully created level {$levelId} at: \"" . realpath(
                    $file
                ) . '"</h2>';
            $statusCode = 201;
        } elseif (isset($_POST)) {
            $result = "<h2 class=\"text-success\">Failed to create level {$levelId} at: \"" . realpath(
                    $file
                ) . '"</h2>';
            $statusCode = 400;
        }
    }
} else {
    $result = 'Nothing processed';
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Result</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container" style="margin-top: 50px">

    <?php if ($result) : ?>
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <?= $result ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-offset-1 col-md-12">
                <a href="index.php" class="btn btn-default btn-lg">
                    Create Level
                </a>
            </div>
        </div>
    <?php endif; ?>
</div>

</body>
</html>

<style>
    html * {
        font-family: 'Open Sans', sans-serif !important;
        color: #777777;
    }

    body {
        background-color: rgb(250, 250, 250) !important;
    }
</style>
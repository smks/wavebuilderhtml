<div class="game-object" data-id="0">
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse0">Snippet 0</a>
                </h4>
            </div>

            <div id="collapse0" class="panel-collapse collapse in">
                <div class="panel-body">
                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="type">Snippet</label>

                        <div class="col-md-4">
                            <select name="objects[0][type]" class="form-control go-type">
                                <?php foreach ($typeList as $key => $snippet) : ?>
                                    <option data-type="<?= $snippet ?>" value="<?= $key ?>"><?= $snippet ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="delayAfter">Delay After</label>

                        <div class="col-md-4">
                            <input name="objects[0][delayAfter]" type="text" placeholder="1200"
                                   class="form-control input-md go-delayAfter" required="" value="0">
                            <span class="help-block">How long to wait until next object?</span>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="speed">Speed</label>

                        <div class="col-md-4">
                            <input name="objects[0][speed]" type="text" placeholder="1"
                                   class="form-control input-md go-speed" required="" value="1">
                            <span class="help-block">How fast should it move (1) default normal speed</span>
                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="grounded">Grounded</label>

                        <div class="col-md-4">
                            <input name="objects[0][grounded]" type="checkbox" class="checkbox go-grounded">
                            <span class="help-block">Should the player always appear above the object</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
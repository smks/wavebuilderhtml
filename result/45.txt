## Wave - 45
Obstacle Count: 250
Corn Count: 25
Snippets used for creation: array(42) {
  [0]=>
  string(67) "game-object-snippets/_fox-sit-vertical-3-fox-runthrough-bottom.json"
  [1]=>
  string(38) "game-object-snippets/_rock-open-1.json"
  [2]=>
  string(49) "game-object-snippets/_rock-vertical-corn-top.json"
  [3]=>
  string(49) "game-object-snippets/_rock-vertical-corn-top.json"
  [4]=>
  string(49) "game-object-snippets/_rock-vertical-corn-top.json"
  [5]=>
  string(48) "game-object-snippets/_orb-1-corn-arrow-left.json"
  [6]=>
  string(49) "game-object-snippets/_mud-vertical-missing-2.json"
  [7]=>
  string(51) "game-object-snippets/_rock-capture-top-depth-3.json"
  [8]=>
  string(57) "game-object-snippets/_mud-vertical-missing-1-2-rocks.json"
  [9]=>
  string(54) "game-object-snippets/_fox-running-vertical-open-3.json"
  [10]=>
  string(49) "game-object-snippets/_mud-vertical-missing-5.json"
  [11]=>
  string(15) "_fox-stand.json"
  [12]=>
  string(60) "game-object-snippets/_hay-diamond-open-top-bottom-holes.json"
  [13]=>
  string(46) "game-object-snippets/_log-2-2-middle-hole.json"
  [14]=>
  string(42) "game-object-snippets/_corn-top-middle.json"
  [15]=>
  string(44) "game-object-snippets/_corn-top-middle-2.json"
  [16]=>
  string(56) "game-object-snippets/_rock-vertical-corn-middle-top.json"
  [17]=>
  string(11) "_bread.json"
  [18]=>
  string(51) "game-object-snippets/_hole-left-semi-circle-10.json"
  [19]=>
  string(46) "game-object-snippets/_hay-vertical-open-4.json"
  [20]=>
  string(38) "game-object-snippets/_rock-open-6.json"
  [21]=>
  string(10) "_hole.json"
  [22]=>
  string(17) "_fox-running.json"
  [23]=>
  string(38) "game-object-snippets/_rock-open-7.json"
  [24]=>
  string(54) "game-object-snippets/_rock-capture-bottom-depth-2.json"
  [25]=>
  string(17) "_fox-running.json"
  [26]=>
  string(47) "game-object-snippets/_hole-vertical-corn-1.json"
  [27]=>
  string(42) "game-object-snippets/_hay-arrow-right.json"
  [28]=>
  string(43) "game-object-snippets/_rock-open-bottom.json"
  [29]=>
  string(15) "_fox-stand.json"
  [30]=>
  string(38) "game-object-snippets/_rock-open-5.json"
  [31]=>
  string(38) "game-object-snippets/_rock-open-7.json"
  [32]=>
  string(57) "game-object-snippets/_mud-vertical-missing-1-2-rocks.json"
  [33]=>
  string(47) "game-object-snippets/_rock-top-bottom-open.json"
  [34]=>
  string(47) "game-object-snippets/_corn-middle-bottom-3.json"
  [35]=>
  string(10) "_hole.json"
  [36]=>
  string(45) "game-object-snippets/_bread-horizontal-3.json"
  [37]=>
  string(10) "_corn.json"
  [38]=>
  string(54) "game-object-snippets/_fox-running-vertical-open-4.json"
  [39]=>
  string(38) "game-object-snippets/_rock-open-8.json"
  [40]=>
  string(10) "_hole.json"
  [41]=>
  string(55) "game-object-snippets/_log-slope-up-open-bottom-top.json"
}


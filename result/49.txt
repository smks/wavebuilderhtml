## Wave - 49
Obstacle Count: 270
Corn Count: 25
Snippets used for creation: array(38) {
  [0]=>
  string(47) "game-object-snippets/_corn-middle-bottom-3.json"
  [1]=>
  string(49) "game-object-snippets/_corn-middle-slope-up-5.json"
  [2]=>
  string(44) "game-object-snippets/_corn-top-middle-3.json"
  [3]=>
  string(9) "_mud.json"
  [4]=>
  string(56) "game-object-snippets/_rock-vertical-corn-middle-top.json"
  [5]=>
  string(54) "game-object-snippets/_rock-capture-middle-depth-2.json"
  [6]=>
  string(38) "game-object-snippets/_rock-open-6.json"
  [7]=>
  string(54) "game-object-snippets/_fox-running-vertical-open-3.json"
  [8]=>
  string(46) "game-object-snippets/_log-zig-zag-up-down.json"
  [9]=>
  string(46) "game-object-snippets/_orb-1-corn-vertical.json"
  [10]=>
  string(15) "_fox-stand.json"
  [11]=>
  string(54) "game-object-snippets/_rock-capture-bottom-depth-2.json"
  [12]=>
  string(54) "game-object-snippets/_fox-running-vertical-open-2.json"
  [13]=>
  string(38) "game-object-snippets/_rock-open-4.json"
  [14]=>
  string(45) "game-object-snippets/_fox-sit-vertical-3.json"
  [15]=>
  string(45) "game-object-snippets/_corn-bottom-middle.json"
  [16]=>
  string(49) "game-object-snippets/_corn-bottom-straight-3.json"
  [17]=>
  string(57) "game-object-snippets/_log-slope-down-open-top-bottom.json"
  [18]=>
  string(57) "game-object-snippets/_log-slope-down-open-top-bottom.json"
  [19]=>
  string(10) "_rock.json"
  [20]=>
  string(67) "game-object-snippets/_fox-sit-vertical-3-fox-runthrough-bottom.json"
  [21]=>
  string(38) "game-object-snippets/_rock-open-2.json"
  [22]=>
  string(46) "game-object-snippets/_hay-vertical-open-1.json"
  [23]=>
  string(38) "game-object-snippets/_rock-open-1.json"
  [24]=>
  string(54) "game-object-snippets/_rock-capture-middle-depth-3.json"
  [25]=>
  string(54) "game-object-snippets/_rock-capture-bottom-depth-3.json"
  [26]=>
  string(15) "_fox-stand.json"
  [27]=>
  string(17) "_fox-running.json"
  [28]=>
  string(43) "game-object-snippets/_rock-open-bottom.json"
  [29]=>
  string(9) "_hay.json"
  [30]=>
  string(54) "game-object-snippets/_rock-capture-bottom-depth-2.json"
  [31]=>
  string(54) "game-object-snippets/_fox-running-vertical-open-6.json"
  [32]=>
  string(55) "game-object-snippets/_log-slope-up-open-bottom-top.json"
  [33]=>
  string(9) "_hay.json"
  [34]=>
  string(9) "_hay.json"
  [35]=>
  string(54) "game-object-snippets/_fox-running-vertical-open-1.json"
  [36]=>
  string(10) "_rock.json"
  [37]=>
  string(17) "_fox-running.json"
}


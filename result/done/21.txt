## Wave - 21
Obstacle Count: 120
Corn Count: 15
Snippets used for creation: array(26) {
  [0]=>
  string(51) "game-object-snippets/_rock-capture-top-depth-1.json"
  [1]=>
  string(41) "game-object-snippets/_hole-wave-down.json"
  [2]=>
  string(54) "game-object-snippets/_rock-capture-bottom-depth-2.json"
  [3]=>
  string(10) "_hole.json"
  [4]=>
  string(9) "_log.json"
  [5]=>
  string(9) "_hay.json"
  [6]=>
  string(9) "_log.json"
  [7]=>
  string(51) "game-object-snippets/_rock-capture-top-depth-1.json"
  [8]=>
  string(10) "_hole.json"
  [9]=>
  string(46) "game-object-snippets/_hay-vertical-open-2.json"
  [10]=>
  string(43) "game-object-snippets/_rock-open-bottom.json"
  [11]=>
  string(11) "_orb-1.json"
  [12]=>
  string(9) "_hay.json"
  [13]=>
  string(39) "game-object-snippets/_hole-wave-up.json"
  [14]=>
  string(10) "_hole.json"
  [15]=>
  string(54) "game-object-snippets/_hay-diamond-open-top-bottom.json"
  [16]=>
  string(9) "_log.json"
  [17]=>
  string(54) "game-object-snippets/_hay-diamond-open-bottom-top.json"
  [18]=>
  string(49) "game-object-snippets/_mud-vertical-missing-1.json"
  [19]=>
  string(46) "game-object-snippets/_log-vertical-open-3.json"
  [20]=>
  string(46) "game-object-snippets/_hay-vertical-open-4.json"
  [21]=>
  string(54) "game-object-snippets/_orb-1-corn-half-circle-left.json"
  [22]=>
  string(42) "game-object-snippets/_corn-middle-top.json"
  [23]=>
  string(44) "game-object-snippets/_hay-vertical-full.json"
  [24]=>
  string(10) "_corn.json"
  [25]=>
  string(46) "game-object-snippets/_log-vertical-open-2.json"
}


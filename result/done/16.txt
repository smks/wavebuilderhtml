## Wave - 16
Obstacle Count: 95
Corn Count: 15
Snippets used for creation: array(22) {
  [0]=>
  string(52) "game-object-snippets/_rock-vertical-corn-bottom.json"
  [1]=>
  string(53) "game-object-snippets/_mud-vertical-middle-bottom.json"
  [2]=>
  string(46) "game-object-snippets/_log-vertical-open-3.json"
  [3]=>
  string(54) "game-object-snippets/_rock-capture-middle-depth-2.json"
  [4]=>
  string(9) "_log.json"
  [5]=>
  string(9) "_mud.json"
  [6]=>
  string(9) "_log.json"
  [7]=>
  string(45) "game-object-snippets/_corn-bottom-middle.json"
  [8]=>
  string(10) "_hole.json"
  [9]=>
  string(48) "game-object-snippets/_orb-1-corn-arrow-left.json"
  [10]=>
  string(46) "game-object-snippets/_log-2-2-middle-hole.json"
  [11]=>
  string(46) "game-object-snippets/_log-vertical-open-1.json"
  [12]=>
  string(46) "game-object-snippets/_log-vertical-open-2.json"
  [13]=>
  string(57) "game-object-snippets/_mud-vertical-missing-1-2-rocks.json"
  [14]=>
  string(57) "game-object-snippets/_mud-vertical-missing-1-2-rocks.json"
  [15]=>
  string(43) "game-object-snippets/_hole-arrow-right.json"
  [16]=>
  string(10) "_rock.json"
  [17]=>
  string(44) "game-object-snippets/_mud-vertical-full.json"
  [18]=>
  string(9) "_log.json"
  [19]=>
  string(45) "game-object-snippets/_hole-vertical-full.json"
  [20]=>
  string(10) "_rock.json"
  [21]=>
  string(11) "_orb-1.json"
}


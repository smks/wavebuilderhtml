## Wave - 33
Obstacle Count: 190
Corn Count: 25
Snippets used for creation: array(30) {
  [0]=>
  string(9) "_log.json"
  [1]=>
  string(10) "_hole.json"
  [2]=>
  string(11) "_bread.json"
  [3]=>
  string(49) "game-object-snippets/_rock-vertical-corn-top.json"
  [4]=>
  string(54) "game-object-snippets/_orb-1-corn-half-circle-left.json"
  [5]=>
  string(49) "game-object-snippets/_mud-vertical-missing-1.json"
  [6]=>
  string(54) "game-object-snippets/_rock-capture-bottom-depth-2.json"
  [7]=>
  string(54) "game-object-snippets/_rock-capture-middle-depth-2.json"
  [8]=>
  string(44) "game-object-snippets/_hole-wave-down-up.json"
  [9]=>
  string(54) "game-object-snippets/_rock-capture-bottom-depth-1.json"
  [10]=>
  string(49) "game-object-snippets/_mud-vertical-missing-5.json"
  [11]=>
  string(45) "game-object-snippets/_hole-vertical-full.json"
  [12]=>
  string(45) "game-object-snippets/_corn-bottom-middle.json"
  [13]=>
  string(11) "_orb-3.json"
  [14]=>
  string(47) "game-object-snippets/_hole-vertical-corn-1.json"
  [15]=>
  string(11) "_orb-2.json"
  [16]=>
  string(53) "game-object-snippets/_rock-vertical-corn-1-2-3-4.json"
  [17]=>
  string(10) "_rock.json"
  [18]=>
  string(17) "_fox-running.json"
  [19]=>
  string(15) "_fox-stand.json"
  [20]=>
  string(39) "game-object-snippets/_hole-wave-up.json"
  [21]=>
  string(49) "game-object-snippets/_mud-vertical-missing-2.json"
  [22]=>
  string(51) "game-object-snippets/_rock-capture-top-depth-3.json"
  [23]=>
  string(45) "game-object-snippets/_corn-middle-bottom.json"
  [24]=>
  string(9) "_mud.json"
  [25]=>
  string(41) "game-object-snippets/_hole-wave-down.json"
  [26]=>
  string(53) "game-object-snippets/_mud-vertical-middle-bottom.json"
  [27]=>
  string(17) "_fox-running.json"
  [28]=>
  string(11) "_bread.json"
  [29]=>
  string(10) "_rock.json"
}


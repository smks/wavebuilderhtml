## Wave - 33
Obstacle Count: 190
Corn Count: 25
Snippets used for creation: array(44) {
  [0]=>
  string(11) "_orb-3.json"
  [1]=>
  string(49) "game-object-snippets/_rock-vertical-corn-top.json"
  [2]=>
  string(46) "game-object-snippets/_hay-vertical-open-2.json"
  [3]=>
  string(54) "game-object-snippets/_rock-capture-bottom-depth-3.json"
  [4]=>
  string(10) "_hole.json"
  [5]=>
  string(47) "game-object-snippets/_corn-middle-straight.json"
  [6]=>
  string(54) "game-object-snippets/_hay-diamond-open-top-bottom.json"
  [7]=>
  string(17) "_fox-running.json"
  [8]=>
  string(9) "_log.json"
  [9]=>
  string(49) "game-object-snippets/_corn-middle-straight-2.json"
  [10]=>
  string(9) "_log.json"
  [11]=>
  string(10) "_rock.json"
  [12]=>
  string(17) "_fox-running.json"
  [13]=>
  string(39) "game-object-snippets/_hole-wave-up.json"
  [14]=>
  string(47) "game-object-snippets/_hole-vertical-corn-1.json"
  [15]=>
  string(9) "_mud.json"
  [16]=>
  string(39) "game-object-snippets/_hole-wave-up.json"
  [17]=>
  string(10) "_corn.json"
  [18]=>
  string(54) "game-object-snippets/_rock-capture-bottom-depth-2.json"
  [19]=>
  string(10) "_corn.json"
  [20]=>
  string(40) "game-object-snippets/_rock-open-top.json"
  [21]=>
  string(42) "game-object-snippets/_corn-top-middle.json"
  [22]=>
  string(47) "game-object-snippets/_corn-middle-straight.json"
  [23]=>
  string(10) "_corn.json"
  [24]=>
  string(41) "game-object-snippets/_hole-wave-down.json"
  [25]=>
  string(47) "game-object-snippets/_corn-bottom-middle-2.json"
  [26]=>
  string(64) "game-object-snippets/_fox-sit-vertical-3-fox-runthrough-top.json"
  [27]=>
  string(45) "game-object-snippets/_hole-vertical-full.json"
  [28]=>
  string(57) "game-object-snippets/_mud-vertical-missing-1-2-rocks.json"
  [29]=>
  string(51) "game-object-snippets/_hole-left-semi-circle-10.json"
  [30]=>
  string(40) "game-object-snippets/_rock-open-top.json"
  [31]=>
  string(47) "game-object-snippets/_rock-top-bottom-open.json"
  [32]=>
  string(10) "_rock.json"
  [33]=>
  string(39) "game-object-snippets/_hole-wave-up.json"
  [34]=>
  string(56) "game-object-snippets/_rock-vertical-corn-middle-top.json"
  [35]=>
  string(57) "game-object-snippets/_mud-vertical-missing-1-2-rocks.json"
  [36]=>
  string(9) "_log.json"
  [37]=>
  string(17) "_fox-running.json"
  [38]=>
  string(10) "_hole.json"
  [39]=>
  string(43) "game-object-snippets/_rock-open-bottom.json"
  [40]=>
  string(9) "_mud.json"
  [41]=>
  string(10) "_rock.json"
  [42]=>
  string(15) "_fox-stand.json"
  [43]=>
  string(9) "_mud.json"
}


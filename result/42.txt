## Wave - 42
Obstacle Count: 235
Corn Count: 25
Snippets used for creation: array(38) {
  [0]=>
  string(46) "game-object-snippets/_hay-vertical-open-1.json"
  [1]=>
  string(46) "game-object-snippets/_log-2-2-middle-hole.json"
  [2]=>
  string(46) "game-object-snippets/_log-vertical-open-3.json"
  [3]=>
  string(57) "game-object-snippets/_mud-vertical-missing-1-2-rocks.json"
  [4]=>
  string(54) "game-object-snippets/_rock-capture-bottom-depth-3.json"
  [5]=>
  string(47) "game-object-snippets/_corn-bottom-middle-2.json"
  [6]=>
  string(41) "game-object-snippets/_hole-wave-down.json"
  [7]=>
  string(10) "_corn.json"
  [8]=>
  string(43) "game-object-snippets/_rock-open-bottom.json"
  [9]=>
  string(9) "_hay.json"
  [10]=>
  string(43) "game-object-snippets/_hole-arrow-right.json"
  [11]=>
  string(46) "game-object-snippets/_log-2-2-middle-hole.json"
  [12]=>
  string(49) "game-object-snippets/_orb-1-corn-full-circle.json"
  [13]=>
  string(46) "game-object-snippets/_corn-top-straight-2.json"
  [14]=>
  string(44) "game-object-snippets/_corn-top-middle-3.json"
  [15]=>
  string(49) "game-object-snippets/_rock-vertical-corn-top.json"
  [16]=>
  string(46) "game-object-snippets/_hay-vertical-open-4.json"
  [17]=>
  string(15) "_fox-stand.json"
  [18]=>
  string(15) "_fox-stand.json"
  [19]=>
  string(43) "game-object-snippets/_rock-open-bottom.json"
  [20]=>
  string(9) "_hay.json"
  [21]=>
  string(9) "_hay.json"
  [22]=>
  string(43) "game-object-snippets/_rock-open-bottom.json"
  [23]=>
  string(64) "game-object-snippets/_fox-sit-vertical-3-fox-runthrough-top.json"
  [24]=>
  string(54) "game-object-snippets/_fox-running-vertical-open-4.json"
  [25]=>
  string(9) "_hay.json"
  [26]=>
  string(15) "_fox-stand.json"
  [27]=>
  string(17) "_fox-running.json"
  [28]=>
  string(54) "game-object-snippets/_fox-running-vertical-open-4.json"
  [29]=>
  string(41) "game-object-snippets/_hole-wave-down.json"
  [30]=>
  string(43) "game-object-snippets/_hole-arrow-right.json"
  [31]=>
  string(45) "game-object-snippets/_hole-vertical-full.json"
  [32]=>
  string(54) "game-object-snippets/_fox-running-vertical-open-3.json"
  [33]=>
  string(51) "game-object-snippets/_rock-capture-top-depth-2.json"
  [34]=>
  string(49) "game-object-snippets/_hole-wave-down-2-rocks.json"
  [35]=>
  string(54) "game-object-snippets/_rock-capture-middle-depth-1.json"
  [36]=>
  string(9) "_log.json"
  [37]=>
  string(41) "game-object-snippets/_hay-arrow-left.json"
}


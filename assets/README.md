# Mobile Game Wave Builder

## I need my mobile game levels to be designed

Looking specifically for a junior who could also learn aspects of how a mobile game is constructed. Also great experience to put on the CV.

I am working on a mobile game and need someone to design the levels for me. I have created so far 10 out of 300. Each level is a JSON file that contains an array of objects that get run one after the other. Each level requires a certain count of objects. E.g.

Wave 1 needs 5 obstacles and 5 corn items.

A full list can be found in the README of the requirements. 

The levels have to get progressively harder. Certain objects should only be used depending on the wave:

wave 1 and up - Corn, Rabbit holes get introduced.
wave 6 and up - Rocks get introduced.
wave 11 and up - Mud and Bread get introduced.
wave 16 and up - Logs and Yellow Orb get introduced.
wave 21 and up - Blue Orb and Hay Bales get introduced.
wave 26 and up - Red Orb gets introduced.

Examples of the objects can be found under: assets/examples

## Required skills:

1) Use git for version control - Bitbucket to pull the repo to your local machine.

2) The ability to run a local server in the root of the repo. E.g. PHP, Python or Node.

3) Understanding of JSON.

### See the README at this url.
https://bitbucket.org/smks/wavebuilderhtml/src/e2b3cac0056af031fe9fde5d3bbfb2b2e3b89a12/assets/README.md?at=master&fileviewer=file-view-default

Name will optionally get a mention in the credits upon release.

There is a visual tool I have built that allows you to view the waves you build.

Thanks!

## Instructions

To build waves we have to create separate JSON Files labelled with the wave ID:

    1.json
    2.json
    3.json
    ...
    300.json

These exist in:


    bin/assets/data/waves

Files need to be created here and contain game objects.

Each wave contains an array of objects that start from the right and move across the screen to the left.

Example: (**1.json**)

    [
	  {
	    "type": "items\/corn",
	    "xPos": "1920",
	    "yPos": "636",
	    "delayBefore": "0",
	    "delayAfter": "400",
	    "speed": "1",
	    "typeId": "3",
	    "grounded": false
	  },
	  {
	    "type": "obstacles\/hole",
	    "xPos": "1920",
	    "yPos": "372",
	    "delayBefore": "0",
	    "delayAfter": "0",
	    "speed": "1",
	    "typeId": "8",
	    "grounded": true
	  }
	]

Above will first show the corn, then the hole after a 400 milliseconds delay.

1920x1080 coordinates to be applied. 

**type:** Needs to be correct string of the asset name

**xPos:** The x coordinate it will start at on screen

**yPos:** The y coordinate it will start at on screen

**delayBefore:** How long in milliseconds to wait before 
showing

**delayAfter:** How long in milliseconds to wait before showing the next object

**speed:** Default (1) The speed the object moves left. 1 Will follow the ground.

**typeId:** The id of the object list (needs to correspond with asset name):

    TREE_1:Int = 0;
    TREE_2:Int = 1;
    BREAD:Int = 2;
    CORN:Int = 3;
    HAY:Int = 4;
    MUD:Int = 5;
    LOG:Int = 6;
    ROCK:Int = 7;
    HOLE:Int = 8;
    FOX_RUNNING:Int = 9;
    FOX_SIT:Int = 10;
    ORB_1:Int = 11;
    ORB_2:Int = 12;
    ORB_3:Int = 13;

The idea is to make these waves fun to play. Imagine the player using the keyboard/thumbstick to move through these waves while progressively getting more difficult.

Here is the required count of corn and obstacles to be shown for each wave:

| Wave | Corn | Bread | Obstacles | Orbs |
|------|------|-------|-----------|------|
| 1    | 5    | 0     | 5         | 0    |
| 2    | 5    | 0     | 10        | 0    |
| 3    | 5    | 0     | 15        | 0    |
| 4    | 5    | 0     | 20        | 0    |
| 5    | 5    | 0     | 25        | 0    |
| 6    | 5    | 0     | 30        | 0    |
| 7    | 5    | 0     | 35        | 0    |
| 8    | 5    | 0     | 40        | 0    |
| 9    | 5    | 0     | 45        | 0    |
| 10   | 5    | 3     | 50        | 0    |
| 11   | 15   | 0     | 55        | 1    |
| 12   | 15   | 0     | 60        | 1    |
| 13   | 15   | 0     | 65        | 1    |
| 14   | 15   | 0     | 70        | 1    |
| 15   | 15   | 0     | 75        | 1    |
| 16   | 15   | 0     | 80        | 1    |
| 17   | 15   | 0     | 85        | 1    |
| 18   | 15   | 0     | 90        | 1    |
| 19   | 15   | 0     | 95        | 1    |
| 20   | 15   | 3     | 100       | 1    |
| 21   | 15   | 0     | 105       | 1    |
| 22   | 15   | 0     | 110       | 1    |
| 23   | 15   | 0     | 115       | 1    |
| 24   | 15   | 0     | 120       | 1    |
| 25   | 15   | 0     | 125       | 1    |
| 26   | 15   | 0     | 130       | 1    |
| 27   | 15   | 0     | 135       | 1    |
| 28   | 15   | 0     | 140       | 1    |
| 29   | 15   | 0     | 145       | 1    |
| 30   | 15   | 3     | 150       | 1    |
| 31   | 25   | 0     | 155       | 1    |
| 32   | 25   | 0     | 160       | 1    |
| 33   | 25   | 0     | 165       | 1    |
| 34   | 25   | 0     | 170       | 1    |
| 35   | 25   | 0     | 175       | 1    |
| 36   | 25   | 0     | 180       | 1    |
| 37   | 25   | 0     | 185       | 1    |
| 38   | 25   | 0     | 190       | 1    |
| 39   | 25   | 0     | 195       | 1    |
| 40   | 25   | 3     | 200       | 1    |
| 41   | 25   | 0     | 205       | 1    |
| 42   | 25   | 0     | 210       | 1    |
| 43   | 25   | 0     | 215       | 1    |
| 44   | 25   | 0     | 220       | 1    |
| 45   | 25   | 0     | 225       | 1    |
| 46   | 25   | 0     | 230       | 1    |
| 47   | 25   | 0     | 235       | 1    |
| 48   | 25   | 0     | 240       | 1    |
| 49   | 25   | 0     | 245       | 1    |
| 50   | 25   | 3     | 250       | 1    |
| 51   | 30   | 0     | 255       | 2    |
| 52   | 30   | 0     | 260       | 2    |
| 53   | 30   | 0     | 265       | 2    |
| 54   | 30   | 0     | 270       | 2    |
| 55   | 30   | 0     | 275       | 2    |
| 56   | 30   | 0     | 280       | 2    |
| 57   | 30   | 0     | 285       | 2    |
| 58   | 30   | 0     | 290       | 2    |
| 59   | 30   | 0     | 295       | 2    |
| 60   | 30   | 3     | 300       | 2    |
| 61   | 30   | 0     | 305       | 2    |
| 62   | 30   | 0     | 310       | 2    |
| 63   | 30   | 0     | 315       | 2    |
| 64   | 30   | 0     | 320       | 2    |
| 65   | 30   | 0     | 325       | 2    |
| 66   | 30   | 0     | 330       | 2    |
| 67   | 30   | 0     | 335       | 2    |
| 68   | 30   | 0     | 340       | 2    |
| 69   | 30   | 0     | 345       | 2    |
| 70   | 30   | 3     | 350       | 2    |
| 71   | 30   | 0     | 355       | 2    |
| 72   | 30   | 0     | 360       | 2    |
| 73   | 30   | 0     | 365       | 2    |
| 74   | 30   | 0     | 370       | 2    |
| 75   | 30   | 0     | 375       | 2    |
| 76   | 30   | 0     | 380       | 2    |
| 77   | 30   | 0     | 385       | 2    |
| 78   | 30   | 0     | 390       | 2    |
| 79   | 30   | 0     | 395       | 2    |
| 80   | 30   | 3     | 400       | 2    |
| 81   | 35   | 0     | 405       | 2    |
| 82   | 35   | 0     | 410       | 2    |
| 83   | 35   | 0     | 415       | 2    |
| 84   | 35   | 0     | 420       | 2    |
| 85   | 35   | 0     | 425       | 2    |
| 86   | 35   | 0     | 430       | 2    |
| 87   | 35   | 0     | 435       | 2    |
| 88   | 35   | 0     | 440       | 2    |
| 89   | 35   | 0     | 445       | 2    |
| 90   | 35   | 3     | 450       | 2    |
| 91   | 35   | 0     | 455       | 2    |
| 92   | 35   | 0     | 460       | 2    |
| 93   | 35   | 0     | 465       | 2    |
| 94   | 35   | 0     | 470       | 2    |
| 95   | 35   | 0     | 475       | 2    |
| 96   | 35   | 0     | 480       | 2    |
| 97   | 35   | 0     | 485       | 2    |
| 98   | 35   | 0     | 490       | 2    |
| 99   | 35   | 0     | 495       | 2    |
| 100  | 35   | 3     | 500       | 2    |
| 101  | 45   | 0     | 505       | 3    |
| 102  | 45   | 0     | 510       | 3    |
| 103  | 45   | 0     | 515       | 3    |
| 104  | 45   | 0     | 520       | 3    |
| 105  | 45   | 0     | 525       | 3    |
| 106  | 45   | 0     | 530       | 3    |
| 107  | 45   | 0     | 535       | 3    |
| 108  | 45   | 0     | 540       | 3    |
| 109  | 45   | 0     | 545       | 3    |
| 110  | 45   | 3     | 550       | 3    |
| 111  | 45   | 0     | 555       | 3    |
| 112  | 45   | 0     | 560       | 3    |
| 113  | 45   | 0     | 565       | 3    |
| 114  | 45   | 0     | 570       | 3    |
| 115  | 45   | 0     | 575       | 3    |
| 116  | 45   | 0     | 580       | 3    |
| 117  | 45   | 0     | 585       | 3    |
| 118  | 45   | 0     | 590       | 3    |
| 119  | 45   | 0     | 595       | 3    |
| 120  | 45   | 3     | 600       | 3    |
| 121  | 45   | 0     | 605       | 3    |
| 122  | 45   | 0     | 610       | 3    |
| 123  | 45   | 0     | 615       | 3    |
| 124  | 45   | 0     | 620       | 3    |
| 125  | 45   | 0     | 625       | 3    |
| 126  | 45   | 0     | 630       | 3    |
| 127  | 45   | 0     | 635       | 3    |
| 128  | 45   | 0     | 640       | 3    |
| 129  | 45   | 0     | 645       | 3    |
| 130  | 45   | 3     | 650       | 3    |
| 131  | 45   | 0     | 655       | 3    |
| 132  | 45   | 0     | 660       | 3    |
| 133  | 45   | 0     | 665       | 3    |
| 134  | 45   | 0     | 670       | 3    |
| 135  | 45   | 0     | 675       | 3    |
| 136  | 45   | 0     | 680       | 3    |
| 137  | 45   | 0     | 685       | 3    |
| 138  | 45   | 0     | 690       | 3    |
| 139  | 45   | 0     | 695       | 3    |
| 140  | 45   | 3     | 700       | 3    |
| 141  | 45   | 0     | 705       | 3    |
| 142  | 45   | 0     | 710       | 3    |
| 143  | 45   | 0     | 715       | 3    |
| 144  | 45   | 0     | 720       | 3    |
| 145  | 45   | 0     | 725       | 3    |
| 146  | 45   | 0     | 730       | 3    |
| 147  | 45   | 0     | 735       | 3    |
| 148  | 45   | 0     | 740       | 3    |
| 149  | 45   | 0     | 745       | 3    |
| 150  | 45   | 3     | 750       | 3    |
| 151  | 55   | 0     | 755       | 4    |
| 152  | 55   | 0     | 760       | 4    |
| 153  | 55   | 0     | 765       | 4    |
| 154  | 55   | 0     | 770       | 4    |
| 155  | 55   | 0     | 775       | 4    |
| 156  | 55   | 0     | 780       | 4    |
| 157  | 55   | 0     | 785       | 4    |
| 158  | 55   | 0     | 790       | 4    |
| 159  | 55   | 0     | 795       | 4    |
| 160  | 55   | 3     | 800       | 4    |
| 161  | 55   | 0     | 805       | 4    |
| 162  | 55   | 0     | 810       | 4    |
| 163  | 55   | 0     | 815       | 4    |
| 164  | 55   | 0     | 820       | 4    |
| 165  | 55   | 0     | 825       | 4    |
| 166  | 55   | 0     | 830       | 4    |
| 167  | 55   | 0     | 835       | 4    |
| 168  | 55   | 0     | 840       | 4    |
| 169  | 55   | 0     | 845       | 4    |
| 170  | 55   | 3     | 850       | 4    |
| 171  | 55   | 0     | 855       | 4    |
| 172  | 55   | 0     | 860       | 4    |
| 173  | 55   | 0     | 865       | 4    |
| 174  | 55   | 0     | 870       | 4    |
| 175  | 55   | 0     | 875       | 4    |
| 176  | 55   | 0     | 880       | 4    |
| 177  | 55   | 0     | 885       | 4    |
| 178  | 55   | 0     | 890       | 4    |
| 179  | 55   | 0     | 895       | 4    |
| 180  | 55   | 3     | 900       | 4    |
| 181  | 55   | 0     | 905       | 4    |
| 182  | 55   | 0     | 910       | 4    |
| 183  | 55   | 0     | 915       | 4    |
| 184  | 55   | 0     | 920       | 4    |
| 185  | 55   | 0     | 925       | 4    |
| 186  | 55   | 0     | 930       | 4    |
| 187  | 55   | 0     | 935       | 4    |
| 188  | 55   | 0     | 940       | 4    |
| 189  | 55   | 0     | 945       | 4    |
| 190  | 55   | 3     | 950       | 4    |
| 191  | 55   | 0     | 955       | 4    |
| 192  | 55   | 0     | 960       | 4    |
| 193  | 55   | 0     | 965       | 4    |
| 194  | 55   | 0     | 970       | 4    |
| 195  | 55   | 0     | 975       | 4    |
| 196  | 55   | 0     | 980       | 4    |
| 197  | 55   | 0     | 985       | 4    |
| 198  | 55   | 0     | 990       | 4    |
| 199  | 55   | 0     | 995       | 4    |
| 200  | 55   | 3     | 1000      | 4    |
| 201  | 60   | 0     | 1005      | 5    |
| 202  | 60   | 0     | 1010      | 5    |
| 203  | 60   | 0     | 1015      | 5    |
| 204  | 60   | 0     | 1020      | 5    |
| 205  | 60   | 0     | 1025      | 5    |
| 206  | 60   | 0     | 1030      | 5    |
| 207  | 60   | 0     | 1035      | 5    |
| 208  | 60   | 0     | 1040      | 5    |
| 209  | 60   | 0     | 1045      | 5    |
| 210  | 60   | 3     | 1050      | 5    |
| 211  | 60   | 0     | 1055      | 5    |
| 212  | 60   | 0     | 1060      | 5    |
| 213  | 60   | 0     | 1065      | 5    |
| 214  | 60   | 0     | 1070      | 5    |
| 215  | 60   | 0     | 1075      | 5    |
| 216  | 60   | 0     | 1080      | 5    |
| 217  | 60   | 0     | 1085      | 5    |
| 218  | 60   | 0     | 1090      | 5    |
| 219  | 60   | 0     | 1095      | 5    |
| 220  | 60   | 3     | 1100      | 5    |
| 221  | 60   | 0     | 1105      | 5    |
| 222  | 60   | 0     | 1110      | 5    |
| 223  | 60   | 0     | 1115      | 5    |
| 224  | 60   | 0     | 1120      | 5    |
| 225  | 60   | 0     | 1125      | 5    |
| 226  | 60   | 0     | 1130      | 5    |
| 227  | 60   | 0     | 1135      | 5    |
| 228  | 60   | 0     | 1140      | 5    |
| 229  | 60   | 0     | 1145      | 5    |
| 230  | 60   | 3     | 1150      | 5    |
| 231  | 60   | 0     | 1155      | 5    |
| 232  | 60   | 0     | 1160      | 5    |
| 233  | 60   | 0     | 1165      | 5    |
| 234  | 60   | 0     | 1170      | 5    |
| 235  | 60   | 0     | 1175      | 5    |
| 236  | 60   | 0     | 1180      | 5    |
| 237  | 60   | 0     | 1185      | 5    |
| 238  | 60   | 0     | 1190      | 5    |
| 239  | 60   | 0     | 1195      | 5    |
| 240  | 60   | 3     | 1200      | 5    |
| 241  | 60   | 0     | 1205      | 5    |
| 242  | 60   | 0     | 1210      | 5    |
| 243  | 60   | 0     | 1215      | 5    |
| 244  | 60   | 0     | 1220      | 5    |
| 245  | 60   | 0     | 1225      | 5    |
| 246  | 60   | 0     | 1230      | 5    |
| 247  | 60   | 0     | 1235      | 5    |
| 248  | 60   | 0     | 1240      | 5    |
| 249  | 60   | 0     | 1245      | 5    |
| 250  | 60   | 3     | 1250      | 5    |
| 251  | 65   | 0     | 1255      | 5    |
| 252  | 65   | 0     | 1260      | 5    |
| 253  | 65   | 0     | 1265      | 5    |
| 254  | 65   | 0     | 1270      | 5    |
| 255  | 65   | 0     | 1275      | 5    |
| 256  | 65   | 0     | 1280      | 5    |
| 257  | 65   | 0     | 1285      | 5    |
| 258  | 65   | 0     | 1290      | 5    |
| 259  | 65   | 0     | 1295      | 5    |
| 260  | 65   | 3     | 1300      | 5    |
| 261  | 65   | 0     | 1305      | 5    |
| 262  | 65   | 0     | 1310      | 5    |
| 263  | 65   | 0     | 1315      | 5    |
| 264  | 65   | 0     | 1320      | 5    |
| 265  | 65   | 0     | 1325      | 5    |
| 266  | 65   | 0     | 1330      | 5    |
| 267  | 65   | 0     | 1335      | 5    |
| 268  | 65   | 0     | 1340      | 5    |
| 269  | 65   | 0     | 1345      | 5    |
| 270  | 65   | 3     | 1350      | 5    |
| 271  | 65   | 0     | 1355      | 5    |
| 272  | 65   | 0     | 1360      | 5    |
| 273  | 65   | 0     | 1365      | 5    |
| 274  | 65   | 0     | 1370      | 5    |
| 275  | 65   | 0     | 1375      | 5    |
| 276  | 65   | 0     | 1380      | 5    |
| 277  | 65   | 0     | 1385      | 5    |
| 278  | 65   | 0     | 1390      | 5    |
| 279  | 65   | 0     | 1395      | 5    |
| 280  | 65   | 3     | 1400      | 5    |
| 281  | 65   | 0     | 1405      | 5    |
| 282  | 65   | 0     | 1410      | 5    |
| 283  | 65   | 0     | 1415      | 5    |
| 284  | 65   | 0     | 1420      | 5    |
| 285  | 65   | 0     | 1425      | 5    |
| 286  | 65   | 0     | 1430      | 5    |
| 287  | 65   | 0     | 1435      | 5    |
| 288  | 65   | 0     | 1440      | 5    |
| 289  | 65   | 0     | 1445      | 5    |
| 290  | 65   | 3     | 1450      | 5    |
| 291  | 65   | 0     | 1455      | 5    |
| 292  | 65   | 0     | 1460      | 5    |
| 293  | 65   | 0     | 1465      | 5    |
| 294  | 65   | 0     | 1470      | 5    |
| 295  | 65   | 0     | 1475      | 5    |
| 296  | 65   | 0     | 1480      | 5    |
| 297  | 65   | 0     | 1485      | 5    |
| 298  | 65   | 0     | 1490      | 5    |
| 299  | 65   | 0     | 1495      | 5    |
| 300  | 65   | 3     | 1500      | 5    |

This has all be set up for unit testing, so when running PHPUNIT I can see that all criteria in table will match.

You can preview how the level looks by loading a server in the root of the project where index.html resides. Python, PHP or node.

    php -S localhost:9000
    
Open up localhost, choose the wave you have created and watch the objects move across the screen.

Thanks!
Shaun.
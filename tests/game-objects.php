<?php
/**
 * Created by PhpStorm.
 * User: Shaun Stone
 * Date: 20/12/2015
 * Time: 14:46
 */
$GAME_OBJ_TREE_1 = 0;
$GAME_OBJ_TREE_2 = 1;
$GAME_OBJ_BREAD = 2;
$GAME_OBJ_CORN = 3;
$GAME_OBJ_HAY = 4;
$GAME_OBJ_MUD = 5;
$GAME_OBJ_LOG = 6;
$GAME_OBJ_ROCK = 7;
$GAME_OBJ_HOLE = 8;
$GAME_OBJ_FOX_RUNNING = 9;
$GAME_OBJ_FOX_SIT = 10;
$GAME_OBJ_YELLOW_ORB = 11;
$GAME_OBJ_BLUE_ORB = 12;
$GAME_OBJ_RED_ORB = 13;

return [
    'backgrounds/tree-1' => $GAME_OBJ_TREE_1,
    'backgrounds/tree-2' => $GAME_OBJ_TREE_2,
    'items/bread' => $GAME_OBJ_BREAD,
    'items/corn' => $GAME_OBJ_CORN,
    'obstacles/hay' => $GAME_OBJ_HAY,
    'obstacles/mud' => $GAME_OBJ_MUD,
    'obstacles/log' => $GAME_OBJ_LOG,
    'obstacles/rock' => $GAME_OBJ_ROCK,
    'obstacles/hole' => $GAME_OBJ_HOLE,
    'enemies/fox-running' => $GAME_OBJ_FOX_RUNNING,
    'enemies/fox-stand' => $GAME_OBJ_FOX_SIT,
    'items/orb-1' => $GAME_OBJ_YELLOW_ORB,
    'items/orb-2' => $GAME_OBJ_BLUE_ORB,
    'items/orb-3' => $GAME_OBJ_RED_ORB
];
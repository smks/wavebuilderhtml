<?php

/**
 * Created by PhpStorm.
 * User: shaunmstone
 * Date: 08/12/15
 * Time: 11:09
 */
class WaveHelper
{
    const PATH = '/../assets/data/waves';
    const TYPE_TREE_1 = 0;
    const TYPE_TREE_2 = 1;
    const TYPE_BREAD = 2;
    const TYPE_CORN = 3;
    const TYPE_ORB1 = 11;
    const TYPE_ORB2 = 12;
    const TYPE_ORB3 = 13;

    // Duration of wave
    const MIN_LENGTH = 17000; // 17 seconds min
    const MAX_LENGTH = 120000; // 2 minutes max

    /**
     * @var
     */
    public $config;
    /**
     * @var
     */
    public $files = null;

    /**
     * @return array|null
     */
    public function getFiles()
    {
        if ($this->files != null) {
            return $this->files;
        }

        $this->files = scandir(__DIR__ . WaveHelper::PATH);

        return $this->files;
    }

    /**
     * @var array
     */
    public $notObstacles = [
        self::TYPE_CORN,
        self::TYPE_TREE_1,
        self::TYPE_TREE_2
    ];

    /**
     * @var
     */
    public $fileCount = null;

      /**
     * @param $id
     * @param $gameObj
     * @return bool
     */
    public function gameObjectDoesNotExitInWave($id, $gameObj)
    {
        $content = $this->getFileById($id);

        if ($content === null) {
            return 0;
        }

        foreach ($content as $go) {

            if ($go->type == $gameObj) {
                return false;
            }

        }

        return true;
    }

    /**
     * @return mixed
     */
    public function fileCount()
    {
        if ($this->fileCount != null) {
            return $this->fileCount;
        }

        $countFiles = function () {
            $l = 0;
            foreach ($this->files as $f) {
                if (strstr($f, '.json')) {
                    ++$l;
                }
            }
            return $l;
        };
        $len = $countFiles();
        $this->fileCount = $len;

        return $len;
    }

    public function getGameObjectCountByType($fileId, $typeId)
    {
        $content = $this->getFileById($fileId);

        if ($content === null) {
            return 0;
        }

        $objectCount = 0;

        foreach ($content as $go) {

            // orb counts (3 types)
            if ($typeId > 10 && (int)$go->typeId > 10) {
                $objectCount++;
            } elseif ($go->typeId == $typeId) {
                $objectCount++;
            }

        }

        return $objectCount;
    }

    /**
     * @param $fileId
     * @return mixed
     */
    public function getFileById($fileId)
    {
        $content = json_decode(
            file_get_contents(__DIR__ . self::PATH . '/' . $fileId . '.json')
        );
        return $content;
    }

    /**
     * @param $fileId
     * @return int
     */
    public function getTotalObstacles($fileId)
    {
        $content = $this->getFileById($fileId);

        if ($content === null) {
            return 0;
        }

        $obstacleCount = 0;

        foreach ($content as $go) {

            if (!in_array($go->typeId, $this->notObstacles)) {
                $obstacleCount++;
            }

        }

        return $obstacleCount;
    }

    /**
     * @param $id
     * @return int
     */
    public function getTotalDurationOfWave($id)
    {
        $content = $this->getFileById($id);

        if ($content === null) {
            return 0;
        }

        $duration = 0;

        foreach ($content as $go) {

            $duration += (int)$go->delayBefore;
            $duration += (int)$go->delayAfter;

        }

        return $duration;
    }
}
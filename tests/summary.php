<?php

ini_set('error_reporting', E_ALL); // or error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');

require_once 'WaveHelper.php';

/**
 * Created by PhpStorm.
 * User: shaunmstone
 * Date: 08/12/15
 * Time: 11:57
 */

$helper = new WaveHelper();
$helper->getFiles();
$length = $helper->fileCount();

echo str_repeat('-', 30) . PHP_EOL;
echo '---- WAVE BUILDER SUMMARY ----' . PHP_EOL;
echo str_repeat('-', 30) . PHP_EOL;
echo sprintf('Total waves progress so far: %s/%s' . PHP_EOL , $length - 1, 300);
echo str_repeat('-', 30) . PHP_EOL;

for ($i = 1; $i < $length; $i++) {
    $cornCount = $helper->getGameObjectCountByType($i, WaveHelper::TYPE_CORN);
    $obstaclesCount = $helper->getTotalObstacles($i);
    $duration = $helper->getTotalDurationOfWave($i);

    echo sprintf('Wave %s has a corn count of: %s' . PHP_EOL , $i, $cornCount);
    echo sprintf('Wave %s has a obstacle count of: %s' . PHP_EOL , $i, $obstaclesCount);
    echo sprintf('Wave %s has a total duration of: %s milliseconds' . PHP_EOL , $i, $duration);
    echo str_repeat('-', 30) . PHP_EOL;
}
<?php

ini_set('error_reporting', E_ALL); // or error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');

require_once 'WaveHelper.php';

// TREE_1:Int = 0;
// TREE_2:Int = 1;
// BREAD:Int = 2;
// CORN:Int = 3;
// HAY:Int = 4;
// MUD:Int = 5;
// LOG:Int = 6;
// ROCK:Int = 7;
// HOLE:Int = 8;
// FOX_RUNNING:Int = 9;
// FOX_SIT:Int = 10;
// ORB_1:Int = 11; yellow
// ORB_2:Int = 12; blue
// ORB_3:Int = 13; red


/**
 *
 * Used to check all existing waves meet requirements
 *
 * Created by PhpStorm.
 * User: Shaun Stone
 * Date: 05/12/2015
 * Time: 16:21
 */
class WaveTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var
     */
    private $config;
    private $files = null;

    /**
     * @var WaveHelper
     */
    private $helper = null;
    /**
     * @var
     */
    private $gameObjects;

    public function setup()
    {
        $this->helper = new WaveHelper();
        $this->config = require __DIR__ . '/../config/waveStats.php';
        $this->gameObjects = require 'game-objects.php';
        $this->helper->getFiles();
    }

    public function testConfigExists()
    {
        $this->assertTrue(is_array($this->config));
    }

    public function testAllCornCounts()
    {
        $len = $this->helper->fileCount();

        for ($i = 1; $i < $len; $i++) {

            $expectedCorn = $this->config[$i]['corn'];

            $cornCount = $this->helper->getGameObjectCountByType($i, WaveHelper::TYPE_CORN);

            $this->assertEquals($expectedCorn, $cornCount, 'Corn count does not match on wave: ' . $i);

        }

        $this->assertSame($i, $len);
    }

    public function testAllBreadCounts()
    {
        $len = $this->helper->fileCount();

        for ($i = 1; $i < $len; $i++) {

            $expectedBread = $this->config[$i]['bread'];

            $breadCount = $this->helper->getGameObjectCountByType($i, WaveHelper::TYPE_BREAD);

            $this->assertEquals($expectedBread, $breadCount, 'Bread count does not match on wave: ' . $i);

        }

        $this->assertSame($i, $len);
    }

    public function testTotalObstacles()
    {
        $len = $this->helper->fileCount();

        for ($i = 1; $i < $len; $i++) {

            $expectedBread = $this->config[$i]['totalObstacles'];

            $breadCount = $this->helper->getTotalObstacles($i);

            $this->assertEquals(
                $expectedBread,
                $breadCount,
                "Total obstacles count {$breadCount} does not match expected {$expectedBread} on wave: " . $i
            );

        }

        $this->assertSame($i, $len);
    }

    public function testAllOrbCounts()
    {
        $len = $this->helper->fileCount();

        for ($i = 1; $i < $len; $i++) {

            $expectedOrb = $this->config[$i]['orbs'];

            $orbCount = $this->helper->getGameObjectCountByType($i, WaveHelper::TYPE_ORB1);

            $this->assertEquals($expectedOrb, $orbCount, 'Orb count does not match on wave: ' . $i);

        }

        $this->assertSame($i, $len);
    }

    public function testDurationOfWave()
    {
        $len = $this->helper->fileCount();

        for ($i = 1; $i < $len; $i++) {

            $expectedMinDuration = WaveHelper::MIN_LENGTH;
            $expectedMinDurationSeconds = $expectedMinDuration / 1000;
            $expectedMaxDuration = WaveHelper::MAX_LENGTH;
            $expectedMaxDurationSeconds = $expectedMaxDuration / 1000;

            $duration = $this->helper->getTotalDurationOfWave($i);
            $durationSeconds = $duration / 1000;

            $this->assertTrue(
                $duration > $expectedMinDuration,
                "Wave {$i} length {$duration} milliseconds (approx {$durationSeconds} seconds) should be longer than: {$expectedMinDuration} milliseconds (approx {$expectedMinDurationSeconds} seconds)"
            );
            $this->assertTrue(
                $duration < $expectedMaxDuration,
                "Wave {$i} length {$duration} milliseconds (approx {$durationSeconds} seconds) should be shorter than: {$expectedMaxDuration} milliseconds (approx {$expectedMaxDurationSeconds} seconds)"
            );

        }

        $this->assertSame($i, $len);
    }

    // wave 1 and up - Corn, Rabbit holes get introduced.
    // wave 6 and up - Rocks get introduced.
    public function testRocksAfterWave5()
    {
        $len = 6;
        $gameObject = 'obstacles/rock';

        for ($i = 1; $i < $len; $i++) {
            $this->assertTrue(
                $this->helper->gameObjectDoesNotExitInWave($i, $gameObject),
                sprintf(
                    '%s should not exist in wave %s',
                    $gameObject,
                    $i
                )
            );
        }
    }

    // wave 11 and up - Mud and Bread get introduced.
    public function testMudAndBreadExistAfterWave10()
    {
        $len = 11;
        $gameObjectMud = 'obstacles/mud';
        $gameObjectBread = 'items/bread';

        for ($i = 1; $i < $len; $i++) {
            $this->assertTrue(
                $this->helper->gameObjectDoesNotExitInWave($i, $gameObjectMud),
                sprintf(
                    '%s should not exist in wave %s',
                    $gameObjectMud,
                    $i
                )
            );
            $this->assertTrue(
                $this->helper->gameObjectDoesNotExitInWave($i, $gameObjectBread),
                sprintf(
                    '%s should not exist in wave %s',
                    $gameObjectBread,
                    $i
                )
            );
        }
    }

    // wave 16 and up - Logs and Yellow Orb get introduced.
    public function testLogsAndRedOrbsExistAfterWave15()
    {
        $len = 16;
        $gameObjectLog = 'obstacles/log';
        $gameObjectOrbOne = 'items/orb-1';

        for ($i = 1; $i < $len; $i++) {
            $this->assertTrue(
                $this->helper->gameObjectDoesNotExitInWave($i, $gameObjectLog),
                sprintf(
                    '%s should not exist in wave %s only at wave %s and greater',
                    $gameObjectLog,
                    $i,
                    $len
                )
            );
            $this->assertTrue(
                $this->helper->gameObjectDoesNotExitInWave($i, $gameObjectOrbOne),
                sprintf(
                    '%s (Yellow Orb) should not exist in wave %s only at wave %s and greater',
                    $gameObjectOrbOne,
                    $i,
                    $len
                )
            );
        }
    }

    // wave 21 and up - Blue Orb and Hay Bales get introduced.
    public function testBlueOrbAndHayBalesIntroduced()
    {

        $len = 21;
        $gameObjectHay = 'obstacles/hay';
        $gameObjectOrbTwo = 'items/orb-2';

        for ($i = 1; $i < $len; $i++) {
            $this->assertTrue(
                $this->helper->gameObjectDoesNotExitInWave($i, $gameObjectHay),
                sprintf(
                    '%s should not exist in wave %s only at wave %s and greater',
                    $gameObjectHay,
                    $i,
                    $len
                )
            );
            $this->assertTrue(
                $this->helper->gameObjectDoesNotExitInWave($i, $gameObjectOrbTwo),
                sprintf(
                    '%s (Blue Orb) should not exist in wave %s only at wave %s and greater',
                    $gameObjectOrbTwo,
                    $i,
                    $len
                )
            );
        }
    }

    // wave 26 and up - Red Orb gets introduced.
    public function testRedOrbAfterWave25()
    {
        $len = 26;
        $gameObject = 'items/orb-3';

        for ($i = 1; $i < $len; $i++) {
            $this->assertTrue(
                $this->helper->gameObjectDoesNotExitInWave($i, $gameObject),
                sprintf(
                    '%s (Red Orb) should not exist in wave %s only at wave 26 and greater',
                    $gameObject,
                    $i
                )
            );
        }
    }

    // wave 31 and up fox sitting and standing
    public function testFoxesIntroduced()
    {
        $len = 30;
        $gameObjectFoxSitting = 'enemies/fox-stand';
        $gameObjectFoxRunning = 'enemies/fox-running';

        for ($i = 1; $i < $len; $i++) {
            $this->assertTrue(
                $this->helper->gameObjectDoesNotExitInWave($i, $gameObjectFoxSitting),
                sprintf(
                    '%s should not exist in wave %s only at wave %s and greater',
                    $gameObjectFoxSitting,
                    $i,
                    $len
                )
            );
            $this->assertTrue(
                $this->helper->gameObjectDoesNotExitInWave($i, $gameObjectFoxRunning),
                sprintf(
                    '%s (Blue Orb) should not exist in wave %s only at wave %s and greater',
                    $gameObjectFoxRunning,
                    $i,
                    $len
                )
            );
        }
    }

    /**
     * @group ownership
     */
    public function testEachObjectOwnsItsCorrectTypeId()
    {
        $len = $this->helper->fileCount();

        for ($i = 1; $i < $len; $i++) {

            $file = $this->helper->getFileById($i);

            foreach ($file as $index => $gO) {

                $name = $gO->type;

                $expectedType = $this->gameObjects[$name];

                $this->assertEquals($expectedType, $gO->typeId, sprintf(
                    'Wave %s - Game Object %s (at position %s) should have the type ID %s and not %s',
                    $i, $name, $index + 1, $expectedType, $gO->typeId
                ));
            }
        }
    }
}

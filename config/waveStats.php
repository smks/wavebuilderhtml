<?php
/**
 * Created by PhpStorm.
 * User: Shaun Stone
 * Date: 05/12/2015
 * Time: 15:46
 */
return [
    0 => [],
    1 => [
        'corn' => 5,
        'bread' => 0,
        'totalObstacles' => 5,
        'orbs' => 0,
        'experience' => 10
    ],
    2 => [
        'corn' => 5,
        'bread' => 0,
        'totalObstacles' => 10,
        'orbs' => 0,
        'experience' => 15
    ],
    3 => [
        'corn' => 5,
        'bread' => 0,
        'totalObstacles' => 15,
        'orbs' => 0,
        'experience' => 20
    ],
    4 => [
        'corn' => 5,
        'bread' => 0,
        'totalObstacles' => 20,
        'orbs' => 0,
        'experience' => 25
    ],
    5 => [
        'corn' => 5,
        'bread' => 0,
        'totalObstacles' => 25,
        'orbs' => 0,
        'experience' => 30
    ],
    6 => [
        'corn' => 5,
        'bread' => 0,
        'totalObstacles' => 30,
        'orbs' => 0,
        'experience' => 35
    ],
    7 => [
        'corn' => 5,
        'bread' => 0,
        'totalObstacles' => 35,
        'orbs' => 0,
        'experience' => 40
    ],
    8 => [
        'corn' => 5,
        'bread' => 0,
        'totalObstacles' => 40,
        'orbs' => 0,
        'experience' => 45
    ],
    9 => [
        'corn' => 5,
        'bread' => 0,
        'totalObstacles' => 45,
        'orbs' => 0,
        'experience' => 50
    ],
    10 => [
        'corn' => 5,
        'bread' => 0,
        'totalObstacles' => 50,
        'orbs' => 0,
        'experience' => 55
    ],
    11 => [
        'corn' => 15,
        'bread' => 3,
        'totalObstacles' => 55,
        'orbs' => 0,
        'experience' => 70
    ],
    12 => [
        'corn' => 15,
        'bread' => 0,
        'totalObstacles' => 60,
        'orbs' => 0,
        'experience' => 75
    ],
    13 => [
        'corn' => 15,
        'bread' => 0,
        'totalObstacles' => 65,
        'orbs' => 0,
        'experience' => 80
    ],
    14 => [
        'corn' => 15,
        'bread' => 0,
        'totalObstacles' => 70,
        'orbs' => 0,
        'experience' => 85
    ],
    15 => [
        'corn' => 15,
        'bread' => 3,
        'totalObstacles' => 75,
        'orbs' => 0,
        'experience' => 90
    ],
    16 => [
        'corn' => 15,
        'bread' => 0,
        'totalObstacles' => 80,
        'orbs' => 1,
        'experience' => 95
    ],
    17 => [
        'corn' => 15,
        'bread' => 0,
        'totalObstacles' => 85,
        'orbs' => 1,
        'experience' => 100
    ],
    18 => [
        'corn' => 15,
        'bread' => 0,
        'totalObstacles' => 90,
        'orbs' => 1,
        'experience' => 105
    ],
    19 => [
        'corn' => 15,
        'bread' => 0,
        'totalObstacles' => 95,
        'orbs' => 1,
        'experience' => 110
    ],
    20 => [
        'corn' => 15,
        'bread' => 3,
        'totalObstacles' => 100,
        'orbs' => 1,
        'experience' => 120
    ],
    21 => [
        'corn' => 15,
        'bread' => 0,
        'totalObstacles' => 105,
        'orbs' => 1,
        'experience' => 120
    ],
    22 => [
        'corn' => 15,
        'bread' => 0,
        'totalObstacles' => 110,
        'orbs' => 1,
        'experience' => 125
    ],
    23 => [
        'corn' => 15,
        'bread' => 0,
        'totalObstacles' => 115,
        'orbs' => 1,
        'experience' => 130
    ],
    24 => [
        'corn' => 15,
        'bread' => 0,
        'totalObstacles' => 120,
        'orbs' => 1,
        'experience' => 135
    ],
    25 => [
        'corn' => 15,
        'bread' => 3,
        'totalObstacles' => 125,
        'orbs' => 1,
        'experience' => 140
    ],
    26 => [
        'corn' => 15,
        'bread' => 0,
        'totalObstacles' => 130,
        'orbs' => 1,
        'experience' => 145
    ],
    27 => [
        'corn' => 15,
        'bread' => 0,
        'totalObstacles' => 135,
        'orbs' => 1,
        'experience' => 150
    ],
    28 => [
        'corn' => 15,
        'bread' => 0,
        'totalObstacles' => 140,
        'orbs' => 1,
        'experience' => 155
    ],
    29 => [
        'corn' => 15,
        'bread' => 0,
        'totalObstacles' => 145,
        'orbs' => 1,
        'experience' => 160
    ],
    30 => [
        'corn' => 15,
        'bread' => 3,
        'totalObstacles' => 150,
        'orbs' => 1,
        'experience' => 170
    ],
    31 => [
        'corn' => 25,
        'bread' => 0,
        'totalObstacles' => 155,
        'orbs' => 1,
        'experience' => 180
    ],
    32 => [
        'corn' => 25,
        'bread' => 0,
        'totalObstacles' => 160,
        'orbs' => 1,
        'experience' => 185
    ],
    33 => [
        'corn' => 25,
        'bread' => 0,
        'totalObstacles' => 165,
        'orbs' => 1,
        'experience' => 190
    ],
    34 => [
        'corn' => 25,
        'bread' => 0,
        'totalObstacles' => 170,
        'orbs' => 1,
        'experience' => 195
    ],
    35 => [
        'corn' => 25,
        'bread' => 3,
        'totalObstacles' => 175,
        'orbs' => 1,
        'experience' => 200
    ],
    36 => [
        'corn' => 25,
        'bread' => 0,
        'totalObstacles' => 180,
        'orbs' => 1,
        'experience' => 205
    ],
    37 => [
        'corn' => 25,
        'bread' => 0,
        'totalObstacles' => 185,
        'orbs' => 1,
        'experience' => 210
    ],
    38 => [
        'corn' => 25,
        'bread' => 0,
        'totalObstacles' => 190,
        'orbs' => 1,
        'experience' => 215
    ],
    39 => [
        'corn' => 25,
        'bread' => 0,
        'totalObstacles' => 195,
        'orbs' => 1,
        'experience' => 220
    ],
    40 => [
        'corn' => 25,
        'bread' => 3,
        'totalObstacles' => 200,
        'orbs' => 1,
        'experience' => 230
    ],
    41 => [
        'corn' => 25,
        'bread' => 0,
        'totalObstacles' => 205,
        'orbs' => 1,
        'experience' => 230
    ],
    42 => [
        'corn' => 25,
        'bread' => 0,
        'totalObstacles' => 210,
        'orbs' => 1,
        'experience' => 235
    ],
    43 => [
        'corn' => 25,
        'bread' => 0,
        'totalObstacles' => 215,
        'orbs' => 1,
        'experience' => 240
    ],
    44 => [
        'corn' => 25,
        'bread' => 0,
        'totalObstacles' => 220,
        'orbs' => 1,
        'experience' => 245
    ],
    45 => [
        'corn' => 25,
        'bread' => 3,
        'totalObstacles' => 225,
        'orbs' => 1,
        'experience' => 250
    ],
    46 => [
        'corn' => 25,
        'bread' => 0,
        'totalObstacles' => 230,
        'orbs' => 1,
        'experience' => 255
    ],
    47 => [
        'corn' => 25,
        'bread' => 0,
        'totalObstacles' => 235,
        'orbs' => 1,
        'experience' => 260
    ],
    48 => [
        'corn' => 25,
        'bread' => 0,
        'totalObstacles' => 240,
        'orbs' => 1,
        'experience' => 265
    ],
    49 => [
        'corn' => 25,
        'bread' => 0,
        'totalObstacles' => 245,
        'orbs' => 1,
        'experience' => 270
    ],
    50 => [
        'corn' => 25,
        'bread' => 0,
        'totalObstacles' => 250,
        'orbs' => 4,
        'experience' => 280
    ],
    51 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 255,
        'orbs' => 2,
        'experience' => 285
    ],
    52 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 260,
        'orbs' => 2,
        'experience' => 290
    ],
    53 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 265,
        'orbs' => 2,
        'experience' => 295
    ],
    54 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 270,
        'orbs' => 2,
        'experience' => 300
    ],
    55 => [
        'corn' => 30,
        'bread' => 3,
        'totalObstacles' => 275,
        'orbs' => 2,
        'experience' => 305
    ],
    56 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 280,
        'orbs' => 2,
        'experience' => 310
    ],
    57 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 285,
        'orbs' => 2,
        'experience' => 315
    ],
    58 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 290,
        'orbs' => 2,
        'experience' => 320
    ],
    59 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 295,
        'orbs' => 2,
        'experience' => 325
    ],
    60 => [
        'corn' => 30,
        'bread' => 3,
        'totalObstacles' => 300,
        'orbs' => 2,
        'experience' => 335
    ],
    61 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 305,
        'orbs' => 2,
        'experience' => 335
    ],
    62 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 310,
        'orbs' => 2,
        'experience' => 340
    ],
    63 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 315,
        'orbs' => 2,
        'experience' => 345
    ],
    64 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 320,
        'orbs' => 2,
        'experience' => 350
    ],
    65 => [
        'corn' => 30,
        'bread' => 3,
        'totalObstacles' => 325,
        'orbs' => 2,
        'experience' => 355
    ],
    66 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 330,
        'orbs' => 2,
        'experience' => 360
    ],
    67 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 335,
        'orbs' => 2,
        'experience' => 365
    ],
    68 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 340,
        'orbs' => 2,
        'experience' => 370
    ],
    69 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 345,
        'orbs' => 2,
        'experience' => 375
    ],
    70 => [
        'corn' => 30,
        'bread' => 3,
        'totalObstacles' => 350,
        'orbs' => 2,
        'experience' => 385
    ],
    71 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 355,
        'orbs' => 2,
        'experience' => 385
    ],
    72 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 360,
        'orbs' => 2,
        'experience' => 390
    ],
    73 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 365,
        'orbs' => 2,
        'experience' => 395
    ],
    74 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 370,
        'orbs' => 2,
        'experience' => 400
    ],
    75 => [
        'corn' => 30,
        'bread' => 3,
        'totalObstacles' => 375,
        'orbs' => 2,
        'experience' => 405
    ],
    76 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 380,
        'orbs' => 2,
        'experience' => 410
    ],
    77 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 385,
        'orbs' => 2,
        'experience' => 415
    ],
    78 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 390,
        'orbs' => 2,
        'experience' => 420
    ],
    79 => [
        'corn' => 30,
        'bread' => 0,
        'totalObstacles' => 395,
        'orbs' => 2,
        'experience' => 425
    ],
    80 => [
        'corn' => 30,
        'bread' => 3,
        'totalObstacles' => 400,
        'orbs' => 2,
        'experience' => 435
    ],
    81 => [
        'corn' => 35,
        'bread' => 0,
        'totalObstacles' => 405,
        'orbs' => 2,
        'experience' => 440
    ],
    82 => [
        'corn' => 35,
        'bread' => 0,
        'totalObstacles' => 410,
        'orbs' => 2,
        'experience' => 445
    ],
    83 => [
        'corn' => 35,
        'bread' => 0,
        'totalObstacles' => 415,
        'orbs' => 2,
        'experience' => 450
    ],
    84 => [
        'corn' => 35,
        'bread' => 0,
        'totalObstacles' => 420,
        'orbs' => 2,
        'experience' => 455
    ],
    85 => [
        'corn' => 35,
        'bread' => 3,
        'totalObstacles' => 425,
        'orbs' => 2,
        'experience' => 460
    ],
    86 => [
        'corn' => 35,
        'bread' => 0,
        'totalObstacles' => 430,
        'orbs' => 2,
        'experience' => 465
    ],
    87 => [
        'corn' => 35,
        'bread' => 0,
        'totalObstacles' => 435,
        'orbs' => 2,
        'experience' => 470
    ],
    88 => [
        'corn' => 35,
        'bread' => 0,
        'totalObstacles' => 440,
        'orbs' => 2,
        'experience' => 475
    ],
    89 => [
        'corn' => 35,
        'bread' => 0,
        'totalObstacles' => 445,
        'orbs' => 2,
        'experience' => 480
    ],
    90 => [
        'corn' => 35,
        'bread' => 3,
        'totalObstacles' => 450,
        'orbs' => 2,
        'experience' => 490
    ],
    91 => [
        'corn' => 35,
        'bread' => 0,
        'totalObstacles' => 455,
        'orbs' => 2,
        'experience' => 490
    ],
    92 => [
        'corn' => 35,
        'bread' => 0,
        'totalObstacles' => 460,
        'orbs' => 2,
        'experience' => 495
    ],
    93 => [
        'corn' => 35,
        'bread' => 0,
        'totalObstacles' => 465,
        'orbs' => 2,
        'experience' => 500
    ],
    94 => [
        'corn' => 35,
        'bread' => 0,
        'totalObstacles' => 470,
        'orbs' => 2,
        'experience' => 505
    ],
    95 => [
        'corn' => 35,
        'bread' => 3,
        'totalObstacles' => 475,
        'orbs' => 2,
        'experience' => 510
    ],
    96 => [
        'corn' => 35,
        'bread' => 0,
        'totalObstacles' => 480,
        'orbs' => 2,
        'experience' => 515
    ],
    97 => [
        'corn' => 35,
        'bread' => 0,
        'totalObstacles' => 485,
        'orbs' => 2,
        'experience' => 520
    ],
    98 => [
        'corn' => 35,
        'bread' => 0,
        'totalObstacles' => 490,
        'orbs' => 2,
        'experience' => 525
    ],
    99 => [
        'corn' => 35,
        'bread' => 0,
        'totalObstacles' => 495,
        'orbs' => 2,
        'experience' => 530
    ],
    100 => [
        'corn' => 35,
        'bread' => 3,
        'totalObstacles' => 500,
        'orbs' => 2,
        'experience' => 540
    ]
];
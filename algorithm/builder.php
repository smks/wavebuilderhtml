<?php
//declare(strict_types = 1);
/*
 * how to use (pass wave number you want to create)
 * php builder.php -w 13
 *
 */
// (1) get wave id from arguments
require __DIR__ . '/functions.php';
require __DIR__ . '/SnippetCostCalculator.php';
require __DIR__ . '/GameObjectCollector.php';
require __DIR__ . '/GameObjectBuilderByFileList.php';
require __DIR__ . '/GameCornCounter.php';

$wave = getWaveOption();
$requiredObstacleCount = getExpectedObstacleCountForWave($wave);
$requiredCornCount = getExpectedCornCountForWave($wave);
$requiredOrbCount = getExpectedOrbCountForWave($wave);
$requiredBreadCount = getExpectedBreadCountForWave($wave);
$isForced = isForceYes();
$isVerbose = isVerbose();

$satisfied = false;
$running = true;
$attempt = 0;

// (1) first calculate count of all game object snippets and costs
$snippetPath = SnippetCostCalculator::SNIPPET_PATH;
$snippetCalculator = new SnippetCostCalculator($wave);
$snippetCalculator->generate();

do {

    // now we need to collect the right amount of objects randomly
    $gameObjectCalculator = new GameObjectCollector(
        $wave,
        $requiredObstacleCount,
        $requiredCornCount,
        $requiredOrbCount,
        $requiredBreadCount
    );
    $list = $gameObjectCalculator->grabAllGameObjectFilenamesByExpectedWaveCount();
    $gameObjectsFilenameList = array_filter(
        $list,
        function ($var) {
            return !is_null($var);
        }
    );

    // Get all files names and construct the json object
    $gameObjectsBuilt = new GameObjectBuilderByFileList($gameObjectsFilenameList);
    $gameObjectsForWave = $gameObjectsBuilt->build();

    // Swap out game objects for corn
    $cornManager = new GameCornCounter($requiredCornCount, $gameObjectsForWave);

    if ($cornManager->hasCorrectAmount()) {
        $running = false;
        $satisfied = true;
    } else {
        logMessage(sprintf('Corn count {%s} did not match expected {%s}', $cornManager->cornCount(), $cornManager->expectedCornCount()), true);
    }

    logMessage(str_repeat('-', 100));
    logMessage(str_repeat('-', 100));
    logMessage('Attempt #' . $attempt, true);
    logMessage(str_repeat('-', 100));
    logMessage(str_repeat('-', 100));

    if ($attempt >= 100000) {
        logMessage("Tried to collect the right corn count but failed after {$attempt} attempts... give it another go");
        $satisfied = true;
        exit();
    }

    $attempt++;

    if ($attempt > 100) {
        //$running = false;
    }

    if ($satisfied === false) {
        $snippetPath = null;
        $snippetCalculator = null;
        $gameObjectCalculator = null;
        $list = null;
        $gameObjectsFilenameList = null;
        $gameObjectsBuilt = null;
        $gameObjectsForWave = null;
        $cornManager = null;
    }

} while ($running === true);

if ($satisfied === false) {
    logMessage('Could not satisfy requirements, aborting');
    exit();
}

if (!$isForced) {
    // Prompt for completion....
    echo json_encode($gameObjectsFilenameList, JSON_PRETTY_PRINT);
    var_dump(sprintf('Total count is %s', count($gameObjectsForWave)));
    var_dump(sprintf('Total corn count is %s', $cornManager->cornCount()));
    logMessage(sprintf('Are you happy to create the above for wave %s? (Y/n)',  $wave), true);
    $decision = fgetc(STDIN);
    if (strtolower($decision) !== 'y') {
        logMessage('Aborting process');
        exit();
    }
}

$file = __DIR__ . '/../assets/data/waves/' . $wave . '.json';
$handle = fopen($file, 'w');
fwrite($handle, json_encode($gameObjectsForWave, JSON_PRETTY_PRINT));
fclose($handle);

// Log results
$logFile = __DIR__ . '/../result/' . $wave . '.txt';
$handle = fopen($logFile, 'w');
$contents = '## Wave - ' . $wave . PHP_EOL;
$contents .= 'Obstacle Count: ' . count($gameObjectsForWave) . PHP_EOL;
$contents .= 'Corn Count: ' . $cornManager->cornCount() . PHP_EOL;
ob_start();
var_dump($gameObjectsFilenameList);
$objectSnippets = ob_get_clean();
$contents .= 'Snippets used for creation: ' . $objectSnippets . PHP_EOL;
fwrite($handle, $contents);
fclose($handle);

logMessage('WOOHOO! Successfully created ' . $file, true);
<?php

/**
 * Created by PhpStorm.
 * User: shaunmstone
 * Date: 08/12/15
 * Time: 11:09
 */
class Helper
{
    const SINGLES_PATH = __DIR__ . '/../assets/examples';

    private $path;
    private $files;

    /**
     * Helper constructor.
     * @param $path
     * @param string $ext
     */
    public function __construct($path, $ext = '.json')
    {
        $this->path = $path;
        $this->files = array_filter(scandir($this->path), function($item) use ($ext) {
            return !is_dir($this->path . '/' . $item) && strstr($item, $ext);
        });
    }


    /**
     * @return array|null
     */
    public function getFiles()
    {
        return array_values($this->files);
    }

    /**
     * @param $fileName
     * @param $gameObj
     * @return bool
     */
    public function gameObjectDoesNotExitInFile($fileName, $gameObj)
    {
        $content = $this->getFileByName($fileName);

        if ($content === null) {
            return 0;
        }

        foreach ($content as $go) {

            if ($go->type == $gameObj) {
                return false;
            }

        }

        return true;
    }

    /**
     * @return mixed
     */
    public function fileCount()
    {
        $countFiles = function () {
            $l = 0;
            foreach ($this->files as $f) {
                if (strstr($f, '.json')) {
                    ++$l;
                }
            }
            return $l;
        };
        return $countFiles();
    }

    /**
     * @param $fileName
     * @return mixed
     */
    public function getFileByName($fileName)
    {
        $content = json_decode(
            file_get_contents($this->path . '/' . $fileName)
        );
        return $content;
    }

    /**
     * @param $gameObjects
     * @return int
     * @throws Exception
     */
    public function getTotalDurationOfFileContents($gameObjects)
    {
        $duration = 0;

        if (is_object($gameObjects)) {
            $duration += (int)$gameObjects->delayBefore;
            $duration += (int)$gameObjects->delayAfter;
            return $duration;
        }

        foreach ($gameObjects as $go) {

            if (!is_object($go)) {
                throw new Exception('not an object: ' . var_dump($gameObjects));
            }

            $duration += (int)$go->delayBefore;
            $duration += (int)$go->delayAfter;

        }

        return $duration;
    }

    public function getCountOfSnippet($gameSnippetFilename)
    {
        $gameObjects = $this->getFileByName($gameSnippetFilename);

        return count($gameObjects);
    }
}
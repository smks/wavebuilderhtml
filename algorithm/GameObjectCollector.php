<?php

require_once 'SnippetCostCalculator.php';

/**
 * Created by PhpStorm.
 * User: Shaun Stone
 * Date: 05/01/2016
 * Time: 20:02
 */
class GameObjectCollector
{
    private $waveId;
    private $gameObjectRequiredCount;
    private $gameObjectCornCount;
    private $requiredOrbCount;
    private $currentCornCount = 0;
    private $results;
    private $complexityMatrix;
    private $helper;
    private $stillBuildingGameObjects = true;
    private $orbCount = 0;
    private $breadCount = 0;
    private $requiredBreadCount;

    /**
     * GameObjectCalculator constructor.
     * @param $waveId
     * @param $gameObjectRequiredCount
     * @param $gameObjectCornCount
     * @param $requiredOrbCount
     * @param $requiredBreadCount
     */
    public function __construct($waveId, $gameObjectRequiredCount, $gameObjectCornCount, $requiredOrbCount, $requiredBreadCount)
    {
        $this->waveId = $waveId;
        $this->gameObjectRequiredCount = $gameObjectRequiredCount;
        $this->gameObjectCornCount = $gameObjectCornCount;
        $this->totalCount = $gameObjectCornCount + $this->gameObjectRequiredCount;
        $this->totalSpent = $gameObjectCornCount + $this->gameObjectRequiredCount;
        $this->results = SnippetCostCalculator::getResults();
        $this->complexityMatrix = $this->generateComplexityMatrix();
        $this->helper = new Helper(Helper::SINGLES_PATH);
        $this->requiredOrbCount = $requiredOrbCount;
        $this->requiredBreadCount = $requiredBreadCount;
    }

    public function grabAllGameObjectFilenamesByExpectedWaveCount()
    {
        $gameObjects = [];
        $i = 0;

        $this->currentCornCount = 0;

        while ($i < ($this->totalCount * 5)) {

            if ($this->totalSpent === 0) {
                break;
            }

            $plucked = $this->pluckGameObject();

            if ($plucked) {
                $gameObjects[] = $plucked;
            }

            $i++;
        }

        logMessage('TOTAL SPEND AT THE END IS: ' . $this->totalSpent);

        return $gameObjects;
    }

    private function pluckGameObject()
    {
        if ($this->totalSpent === 0) {
            return false;
        }

        $cost = $this->getCost();

        logMessage('COST: ' . $cost);

        $allQualifiedSnippetsByComplexity = $this->getAllByComplexity($cost);

        if (empty($allQualifiedSnippetsByComplexity)) {
            $cost = 1;
            $allQualifiedSnippetsByComplexity = $this->getAllByComplexity($cost);
        }

        $gameSnippetKey = array_rand($allQualifiedSnippetsByComplexity, 1);

        $gameSnippet = $allQualifiedSnippetsByComplexity[$gameSnippetKey];

        if (strstr($gameSnippet, 'corn')) {

            $cornSnippet = $this->helper->getFileByName($gameSnippet);

            $cornCount = $this->currentCornCount;

            if (is_array($cornSnippet)) {
                foreach ($cornSnippet as $object) {
                    if ((int)$object->typeId == 3) {
                        $cornCount++;
                    }
                }
            } else {
                $cornCount += 1;
            }

            if ($cornCount > $this->gameObjectCornCount) {

                logMessage('Corn count exceeded: ' . $cornCount . ' > ' . $this->gameObjectRequiredCount, true);

                return false;
            }

            $this->currentCornCount = $cornCount;
        }

        if (strstr($gameSnippet, 'orb')) {

            $totalOrbCount = $this->orbCount + 1;

            if ($totalOrbCount > $this->requiredOrbCount) {
                logMessage('Orb count exceeded: ' . $totalOrbCount . ' > ' . $this->requiredOrbCount, true);
                return false;
            }

            $this->orbCount = $totalOrbCount;
        }

        if (strstr($gameSnippet, 'bread')) {

            $totalBreadCount = $this->breadCount + 1;

            if ($totalBreadCount > $this->requiredBreadCount) {
                logMessage('Bread count exceeded: ' . $totalBreadCount . ' > ' . $this->requiredBreadCount, true);
                return false;
            }

            $this->breadCount = $totalBreadCount;
        }

        if ($gameSnippet === null) {
            throw new \Exception('NO GAME SNIPPET FOUND FOR COST: ' . $cost);
        }

        logMessage('SNIPPET: ' . $gameSnippet);

        $objectCount = $this->helper->getCountOfSnippet($gameSnippet);

        if ($objectCount === 0) {
            throw new \Exception('OBJECT COUNT CANNOT BE 0: ' . $gameSnippet);
        }

        logMessage('SNIPPET SPEND COUNT: ' . $objectCount);

        $spent = $this->totalSpent - $objectCount;

        logMessage('TOTAL LEFT TO SPEND: ' . $this->totalSpent);

        if ($spent < 0) {
            logMessage('COST TOO MUCH - RE-PLUCKING');
            return false;
        }

        if ($spent >= 0 && $gameSnippet) {

            if ($gameSnippet === null) {
               throw new Exception('GAME SNIPPET CANNOT BE NULL');
            }

            $this->totalSpent = $spent;
            logMessage('APPLIED SPENDING TO TOTAL SPEND: ' . $this->totalSpent);
            logMessage('CONTINUING');
            return $gameSnippet;
        }

        $allQualifiedSnippetsByComplexity = null;
        $snippetObjectCountCost = null;
    }

    private function generateComplexityMatrix()
    {
        $complexityMatrix = [];

        $i = 0;
        while ($i < 100) {

            if ($i < 45) {
                $complexityMatrix[$i] = 2;
            } else if ($i < 70 && $i >= 45) {
                $complexityMatrix[$i] = 3;
            } else if ($i < 95 && $i >= 70) {
                $complexityMatrix[$i] = 4;
            } else if ($i >= 95) {
                $complexityMatrix[$i] = 5;
            }

            $i++;
        }

        return $complexityMatrix;
    }

    /**
     * @return mixed
     * @internal param $getRandomCost
     */
    private function getCost()
    {
        $getRandomCost = array_rand($this->complexityMatrix, 1);
        return $this->complexityMatrix[$getRandomCost];
    }

    private function getAllByComplexity($cost)
    {
        $gameObjectIds = [];

        reset($this->results);

        while ($go = current($this->results)) {

            if ($go['complexity'] == $cost) {
                $gameObjectIds[] = key($this->results);
            }
            next($this->results);
        }

        return $gameObjectIds;
    }
}
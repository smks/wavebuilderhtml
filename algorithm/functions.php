<?php
/**
 * Created by PhpStorm.
 * User: Shaun Stone
 * Date: 05/01/2016
 * Time: 18:40
 */

/**
 * @param $message
 * @param $notVerbose
 */
function logMessage($message, $notVerbose = false)
{
    if ($notVerbose) {
        echo $message . PHP_EOL;
    }
}

/**
 * @return mixed
 */
function getWaveOption()
{
    $options = getopt("w:");
    if (!isset($options['w'])) {
        logMessage('Error. Required format: php builder.php -w 12');
        exit();
    }
    $wave = filter_var($options['w'], FILTER_SANITIZE_STRING);
    return $wave;
}

/**
 * @return bool
 */
function isForceYes()
{
    $options = getopt("y:");
    if (!isset($options['y'])) {
        return false;
    }
    return true;
}

/**
 * @return bool
 */
function isVerbose()
{
    $options = getopt("v:");
    if (!isset($options['v'])) {
        return false;
    }
    return true;
}

/**
 * @param $wave
 * @return mixed
 */
function getExpectedObstacleCountForWave($wave)
{
    $waveStates = require __DIR__ . '/../config/waveStats.php';
    return $waveStates[$wave]['totalObstacles'];
}

/**
 * @param $wave
 * @return mixed
 */
function getExpectedCornCountForWave($wave)
{
    $waveStates = require __DIR__ . '/../config/waveStats.php';
    return $waveStates[$wave]['corn'];
}

/**
 * @param $wave
 * @return mixed
 */
function getExpectedOrbCountForWave($wave)
{
    $waveStates = require __DIR__ . '/../config/waveStats.php';
    return $waveStates[$wave]['orbs'];
}

/**
 * @param $wave
 * @return mixed
 */
function getExpectedBreadCountForWave($wave)
{
    $waveStates = require __DIR__ . '/../config/waveStats.php';
    return $waveStates[$wave]['bread'];
}

/**
 * @return float
 */
function getPositionForSnippet()
{
    $randomValue = mt_rand(410, 970);
    return roundUpToFifty($randomValue);
}

/**
 * @param $n
 * @param int $x
 * @return float
 */
function roundUpToFifty($n, $x = 50)
{
    return round(($n + $x / 2) / $x) * $x;
}
<?php

/**
 * Created by PhpStorm.
 * User: Shaun Stone
 * Date: 07/01/2016
 * Time: 19:51
 */
class GameCornCounter
{
    const CORN = 3;

    private $finalCornCount = 0;
    private $requiredCornCount;

    /**
     * GameCornCounter constructor.
     * @param $requiredCornCount
     * @param $gameObjectsForWave
     */
    public function __construct($requiredCornCount, $gameObjectsForWave)
    {
        $this->requiredCornCount = $requiredCornCount;

        foreach ($gameObjectsForWave as $object) {
            if ((int)$object->typeId === self::CORN) {
                $this->finalCornCount++;
            }
        }
    }

    public function cornCount()
    {
        return $this->finalCornCount;
    }

    public function expectedCornCount()
    {
        return $this->requiredCornCount;
    }

    public function hasCorrectAmount()
    {
        return $this->finalCornCount === $this->requiredCornCount;
    }
}
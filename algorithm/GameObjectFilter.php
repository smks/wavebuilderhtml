<?php

/**
 * Created by PhpStorm.
 * User: Shaun Stone
 * Date: 07/01/2016
 * Time: 16:50
 */
class GameObjectFilter
{
    const WAVE_UP_1 = 1;
    const WAVE_UP_6 = 6;
    const WAVE_UP_11 = 11;
    const WAVE_UP_16 = 16;
    const WAVE_UP_21 = 21;
    const WAVE_UP_26 = 26;
    const WAVE_UP_31 = 31;

    private $wave1UpAllowed  = ['corn', 'hole'];
    private $wave6UpAllowed  = ['corn', 'hole', 'rock'];
    private $wave11UpAllowed = ['corn', 'hole', 'rock', 'mud', 'bread'];
    private $wave16UpAllowed = ['corn', 'hole', 'rock', 'mud', 'bread', 'log', 'orb-1'];
    private $wave21UpAllowed = ['corn', 'hole', 'rock', 'mud', 'bread', 'log', 'orb-1', 'orb-2', 'hay'];
    private $wave26UpAllowed = ['corn', 'hole', 'rock', 'mud', 'bread', 'log', 'orb-1', 'orb-2', 'hay', 'orb-3'];
//    private $wave31UpAllowed = ['_corn', '_rock', '_mud', '_bread', '_log', 'orb-1', 'orb-2', 'hay', 'orb-3', 'fox'];
    private $wave31UpAllowed = ['_corn', 'orb-3', 'fox'];

    private $breadWaves = [11, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100];

    /**
     * @param $waveId
     * @param $fileName
     * @return bool
     */
    public function isUsable($waveId, $fileName)
    {
        // bread only available in certain waves
        if (strstr($fileName, 'bread') &&
            !in_array($waveId, $this->breadWaves)) {
            logMessage('Bread cannot be used at wave: ' . $waveId);
            return false;
        }

        if ($waveId >= self::WAVE_UP_31) {
            return $this->partialStringExists($fileName, $this->wave31UpAllowed);
        }

        if ($waveId >= self::WAVE_UP_26) {
            return $this->partialStringExists($fileName, $this->wave26UpAllowed);
        }

        if ($waveId >= self::WAVE_UP_21) {
            return $this->partialStringExists($fileName, $this->wave21UpAllowed);
        }

        if ($waveId >= self::WAVE_UP_16) {
            return $this->partialStringExists($fileName, $this->wave16UpAllowed);
        }

        if ($waveId >= self::WAVE_UP_11) {
            return $this->partialStringExists($fileName, $this->wave11UpAllowed);
        }

        if ($waveId >= self::WAVE_UP_6) {
            return $this->partialStringExists($fileName, $this->wave6UpAllowed);
        }

        if ($waveId >= self::WAVE_UP_1) {
            return $this->partialStringExists($fileName, $this->wave1UpAllowed);
        }

        return false;
    }

    /**
     * @param $fileName
     * @param array $waveGameObjectPartialStrings
     * @return bool
     */
    private function partialStringExists($fileName, array $waveGameObjectPartialStrings)
    {
        foreach ($waveGameObjectPartialStrings as $name) {
            if (strstr($fileName, $name)) {
                return true;
            }
        }

        return false;
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: Shaun Stone
 * Date: 05/01/2016
 * Time: 22:50
 */
class GameObjectBuilderByFileList
{
    /**
     * @var array
     */
    private $gameObjectsFilenameList;
    private $helper;

    /**
     * GameObjectBuilderByFileList constructor.
     * @param array $gameObjectsFilenameList
     */
    public function __construct(array $gameObjectsFilenameList)
    {
        $this->helper = new Helper(Helper::SINGLES_PATH);
        $this->gameObjectsFilenameList = $gameObjectsFilenameList;
    }

    public function build()
    {
        $gameObjects = [];

        foreach ($this->gameObjectsFilenameList as $snippetFile) {

            $file = $this->helper->getFileByName($snippetFile);

            if (is_array($file)) {
                foreach ($file as $key => $val) {
                    $gameObjects[] = $val;
                }
            } else {

                // randomise single object positions
                $file->yPos = getPositionForSnippet();

                $gameObjects[] = $file;
            }
        }

       return array_values($gameObjects);
    }
}
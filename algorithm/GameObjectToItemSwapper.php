<?php

/**
 * Created by PhpStorm.
 * User: Shaun Stone
 * Date: 05/01/2016
 * Time: 22:49
 */
class GameObjectToItemSwapper
{
    private $requiredCornCount;

    /**
     * GameObjectToItemSwapper constructor.
     * @param $requiredCornCount
     */
    public function __construct($requiredCornCount)
    {
        $this->requiredCornCount = $requiredCornCount;
    }
}
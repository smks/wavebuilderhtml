<?php

require 'Helper.php';
require 'GameObjectFilter.php';

/**
 * Created by PhpStorm.
 * User: Shaun Stone
 * Date: 05/01/2016
 * Time: 18:18
 */
class SnippetCostCalculator
{
    const SNIPPET_PATH = __DIR__ . '/../assets/examples/game-object-snippets';
    const SINGLES_PATH = __DIR__ . '/../assets/examples';
    const OUTPUT_PATH = __DIR__ . '/../result';
    const SINGLE_OBJECT = 1;
    const SMALL_GROUP = 2;
    const GROUP = 3;
    const SMALL_BUNDLE = 4;
    const BUNDLE = 5;
    /**
     * @var
     */
    private $helper;
    private $waveId;

    /**
     * SnippetCostCalculator constructor.
     * @param $waveId
     */
    public function __construct($waveId)
    {
        $this->helper = new Helper(self::SNIPPET_PATH);
        $this->singleGameObjectsHelper = new Helper(self::SINGLES_PATH);
        $this->gameObjectFilter = new GameObjectFilter();
        $this->waveId = $waveId;
    }

    public function generate()
    {
        $snippetFiles = $this->helper->getFiles();
        $singleObjectFiles = $this->singleGameObjectsHelper->getFiles();
        $fileCount = $this->helper->fileCount();
        $singleObjectCount = $this->singleGameObjectsHelper->fileCount();
        logMessage('Total game object snippets equals: ' . $fileCount);
        logMessage('Total game objects equals: ' . $singleObjectCount);

        $createdFileContents = [];

        foreach ($snippetFiles as $key => $fileName) {

            if ($this->gameObjectFilter->isUsable($this->waveId, $fileName) === false) {

                logMessage('Excluding snippet ' . $fileName);

                continue;
            }

            logMessage('ADDING: ' . $fileName);

            $gameObjects = $this->helper->getFileByName($fileName);
            $gameObjectsCount = count($gameObjects);
            $gameObjectsDuration = $this->helper->getTotalDurationOfFileContents($gameObjects);
            $createdFileContents['game-object-snippets/' . $fileName] = [
                'count' => $gameObjectsCount,
                'duration' => $gameObjectsDuration,
                'complexity' => $this->getComplexity($gameObjectsCount)
            ];
        }

        foreach ($singleObjectFiles as $fileName) {

            if ($this->gameObjectFilter->isUsable($this->waveId, $fileName) === false) {

                logMessage('Excluding single object ' . $fileName);

                continue;
            }

            logMessage('ADDING: ' . $fileName);

            $gameObjects = $this->singleGameObjectsHelper->getFileByName($fileName);
            $gameObjectsCount = count($gameObjects);
            $gameObjectsDuration = $this->singleGameObjectsHelper->getTotalDurationOfFileContents($gameObjects);
            $createdFileContents[$fileName] = [
                'count' => $gameObjectsCount,
                'duration' => $gameObjectsDuration,
                'complexity' => $this->getComplexity($gameObjectsCount)
            ];
        }

        logMessage('Creating results with a total of: ' . count($createdFileContents));

        $this->createResults($createdFileContents);

    }

    private function getComplexity($gameObjectCount)
    {
        if ($gameObjectCount < 1) {
            throw new Exception('Should not be less than 1!!!!');
            exit;
        }

        if ($gameObjectCount == 1) {
            return self::SINGLE_OBJECT;
        }

        if ($gameObjectCount > 1 && $gameObjectCount <= 5) {
            return self::SMALL_GROUP;
        }

        if ($gameObjectCount > 5 && $gameObjectCount <= 8) {
            return self::GROUP;
        }

        if ($gameObjectCount > 8 && $gameObjectCount <= 15) {
            return self::SMALL_BUNDLE;
        }

        if ($gameObjectCount > 15) {
            return self::BUNDLE;
        }
    }

    private function createResults($createdFileContents)
    {
        logMessage('written results.json to: ' . self::OUTPUT_PATH);
        $handle = fopen(self::getResultsFile(), 'w');
        fwrite($handle, json_encode($createdFileContents, JSON_PRETTY_PRINT));
        fclose($handle);
    }

    public static function getResultsFile()
    {
        return self::OUTPUT_PATH . '/results.json';
    }

    public static function getResults()
    {
        return json_decode(file_get_contents(self::getResultsFile()), true);
    }
}
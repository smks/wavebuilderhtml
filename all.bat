SET wave=%1
for /l %%x in (%wave%, 1, 100) do (
    ECHO 'BUILDING WAVE %%x%'
    php algorithm/builder.php -y1 -w %%x%
)
